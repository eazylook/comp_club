from django.apps import AppConfig


class AppCompClubConfig(AppConfig):
    name = 'app_comp_club'
