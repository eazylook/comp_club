from django.db import models
import uuid
from datetime import datetime, timezone, timedelta
from django.contrib.auth.models import User


dict_places = {
    1: '¹',
    2: ''
}


class TelegramProfile(models.Model):
    chat_id = models.CharField(max_length=255)
    current_action = models.CharField(max_length=255, null=True, blank=True)
    last_select_values = models.TextField(null=True, blank=True)
    about = models.TextField(null=True, blank=True)
    notification_promotion = models.BooleanField(default=True)
    notification_tournament = models.BooleanField(default=True)
    notification_duel = models.BooleanField(default=True)
    selected_sity = models.CharField(max_length=255, null=True, blank=True)
    nick_name = models.CharField(max_length=255, null=True, blank=True)
    discord = models.CharField(max_length=255, null=True, blank=True)
    steam = models.CharField(max_length=255, null=True, blank=True)
    games = models.TextField(null=True, blank=True)
    telegram_nick = models.CharField(max_length=255, null=True, blank=True)
    user = models.OneToOneField(User, related_name='telegram_profile', on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        verbose_name = 'Профиль'
        verbose_name_plural = 'Профили'
        db_table = 'telegram_profiles'

class City(models.Model):
    title = models.CharField(max_length=255, verbose_name='Название города')

    class Meta:
        verbose_name = "Город"
        verbose_name_plural = "Города"
        db_table = 'cities'

    def __str__(self):
        return '{}'.format(self.title)


class Area(models.Model):
    city = models.ForeignKey(City, related_name='ares', on_delete=models.CASCADE)
    title = models.CharField(max_length=255, verbose_name='Название района')

    class Meta:
        verbose_name = "Район"
        verbose_name_plural = "Районы"
        db_table = 'areas'

    def __str__(self):
        return '{}'.format(self.title)


class Tag(models.Model):
    title = models.CharField(max_length=255, verbose_name='Название тега')

    class Meta:
        verbose_name = "Тег"
        verbose_name_plural = "Теги"
        db_table = 'tags'

    def __str__(self):
        return '{}'.format(self.title)


HOUR_CHOICES = (
    (0, 0),
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
    (6, 6),
    (7, 7),
    (8, 8),
    (9, 9),
    (10, 10),
    (11, 11),
    (12, 12),
    (13, 13),
    (14, 14),
    (15, 15),
    (16, 16),
    (17, 17),
    (18, 18),
    (19, 19),
    (20, 20),
    (21, 22),
    (22, 22),
    (23, 23),
)


HOUR_list = (0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23)

class CompClub(models.Model):
    title = models.CharField(max_length=255, verbose_name='Название клуба')
    description = models.TextField(verbose_name='Описание клюба')
    area = models.ForeignKey(Area, related_name='comp_clubs', on_delete=models.CASCADE, blank=True, null=True)
    tags = models.ManyToManyField(Tag, related_name='tags_comp_clubs', null=True, blank=True)
    club_id = models.UUIDField(default=uuid.uuid4, editable=True)
    price = models.DecimalField(max_digits=11, decimal_places=2, blank=True, null=True)
    is_identified = models.BooleanField(default=False, verbose_name='Подтвержден')
    employees = models.ManyToManyField(TelegramProfile, related_name='employees_clubs', null=True, blank=True)
    phone = models.CharField(max_length=255, verbose_name='Телефон для связи', null=True, blank=True)
    club_url = models.URLField(verbose_name='Ссылка ', null=True, blank=True)
    hash_tags = models.CharField(max_length=255, verbose_name='Хеш теги', null=True, blank=True)
    city = models.ForeignKey(City, related_name='city_comp_clubs', null=True, blank=True, on_delete=models.SET_NULL)
    start_work = models.PositiveIntegerField(choices=HOUR_CHOICES, default=0, verbose_name='Начало работы')
    end_work = models.PositiveIntegerField(choices=HOUR_CHOICES, default=0, verbose_name='Окончание работы')

    class Meta:
        verbose_name = "Комп клуб"
        verbose_name_plural = "Комп клубы"
        db_table = 'comp_clubs'

    def __str__(self):
        return '{}'.format(self.title)

class Favorite(models.Model):
    chat_id = models.CharField(max_length=255)
    club = models.ForeignKey(CompClub, related_name='club_favorites', on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Избранное'
        verbose_name_plural = 'Избранное'
        db_table = 'favorites'

    def __str__(self):
        return '{} {}'.format(self.chat_id, self.club)

RATING_GOOD = 'good'
RATING_SHIT = 'shit'

RATING_TYPES = (
    (RATING_GOOD, RATING_GOOD),
    (RATING_SHIT, RATING_SHIT)
)

class Rating(models.Model):
    chat_id = models.CharField(max_length=255)
    club = models.ForeignKey(CompClub, related_name='club_ratings', on_delete=models.CASCADE)
    rating_value = models.CharField(max_length=255, choices=RATING_TYPES)
    text = models.TextField()
    created = models.DateTimeField(blank=True, null=True)

    class Meta:
        verbose_name = 'Рейтинг'
        verbose_name_plural = 'Рейтинг'
        db_table = 'ratings'

    def __str__(self):
        return '{} {} {}'.format(self.chat_id, self.club, self.rating_value)


class FotoClub(models.Model):
    club = models.ForeignKey(CompClub, related_name='club_fotos', on_delete=models.CASCADE)
    img = models.ImageField(upload_to='imgs')

    class Meta:
        verbose_name = 'Фото клуба'
        verbose_name_plural = 'Фото клубов'
        db_table = 'foto_clubs'

    def __str__(self):
        return '{}'.format(self.club)



class PromotionAndDiscount(models.Model):
    title = models.TextField()
    comp_club = models.ForeignKey(CompClub, related_name='promotions_and_discounts', on_delete=models.CASCADE)
    img = models.ImageField(upload_to='imgs', null=True, blank=True)
    likes = models.PositiveIntegerField(default=0, verbose_name='Лайки')

    class Meta:
        verbose_name = "Акции и скидки"
        verbose_name_plural = "Акции и скидки"
        db_table = 'promotions_and_discounts'

    def __str__(self):
        return '{}'.format(self.title)


class PromotionLike(models.Model):
    profile = models.ForeignKey(TelegramProfile, related_name='profile_promotion_likes', on_delete=models.CASCADE)
    promotion = models.ForeignKey(PromotionAndDiscount, related_name='promotion_likes', on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Лайк акции"
        verbose_name_plural = "Лайки акций"
        db_table = 'promotion_likes'

    def __str__(self):
        return '{}'.format(self.profile)


class Tournament(models.Model):
    title = models.TextField()
    comp_club = models.ForeignKey(CompClub, related_name='tournaments', on_delete=models.CASCADE, blank=True, null=True)
    user = models.ForeignKey(TelegramProfile, related_name='user_tournaments', on_delete=models.CASCADE, blank=True, null=True)
    create_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    img = models.ImageField(upload_to='imgs', null=True, blank=True)
    likes = models.PositiveIntegerField(default=0, verbose_name='Лайки')
    date_start = models.DateTimeField(blank=True, null=True, verbose_name='Дата проведения турнира')
    date_notify_participants = models.DateField(blank=True, null=True, verbose_name='Уведомлять участников')

    class Meta:
        verbose_name = "Турнир"
        verbose_name_plural = "Турниры"
        db_table = 'tournaments'

    def __str__(self):
        return '{}'.format(self.title)


class TournamentLike(models.Model):
    profile = models.ForeignKey(TelegramProfile, related_name='profile_tournament_likes', on_delete=models.CASCADE)
    tournament = models.ForeignKey(Tournament, related_name='tournament_likes', on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Лайк tournament"
        verbose_name_plural = "Лайки tournament"
        db_table = 'tournament_likes'

    def __str__(self):
        return '{}'.format(self.profile)


class Duel(models.Model):
    title = models.TextField()
    comp_club = models.ForeignKey(CompClub, related_name='cc_duels', on_delete=models.CASCADE, blank=True, null=True)
    user = models.ForeignKey(TelegramProfile, related_name='user_duels', on_delete=models.CASCADE, blank=True,
                             null=True)
    create_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    number_participants = models.PositiveIntegerField(blank=True, null=True)

    @property
    def get_type(self):
        if self.number_participants:
            return 'Поиск тиммейтов 👥'
        else:
            return 'Дуэль'

    @property
    def get_hours(self):
        end_time = self.create_date + timedelta(hours=24)
        t = end_time - self.create_date
        return int((int(t.total_seconds()) / 60) / 60)

    @property
    def get_create_date(self):
        return '⌛️{}/{}/{}'.format(self.create_date.date().day, self.create_date.date().month, self.create_date.date().year)


    class Meta:
        verbose_name = "Дуэль"
        verbose_name_plural = "Дуэли"
        db_table = 'duels'

    def __str__(self):
        return '{}'.format(self.title)


class Friend(models.Model):
    own = models.CharField(max_length=255)
    friend = models.CharField(max_length=255)

    class Meta:
        verbose_name = "Друг"
        verbose_name_plural = "Друзья"
        db_table = 'friends'

    def __str__(self):
        return '{}'.format(self.own)


class MyTournament(models.Model):
    own = models.CharField(max_length=255)
    tournament_id = models.CharField(max_length=255)

    class Meta:
        verbose_name = "Мой турнир"
        verbose_name_plural = "Мои турниры"
        db_table = 'my_tournaments'

    def __str__(self):
        return '{}'.format(self.own)


class MyDuel(models.Model):
    own = models.CharField(max_length=255)
    duel_id = models.CharField(max_length=255)

    class Meta:
        verbose_name = "Моя дуэль"
        verbose_name_plural = "Мои дуэли"
        db_table = 'my_duels'

    def __str__(self):
        return '{}'.format(self.own)


class Game(models.Model):
    title = models.CharField(max_length=255, verbose_name='Название')

    class Meta:
        verbose_name = "Игра"
        verbose_name_plural = "Игры"
        db_table = 'games'

    def __str__(self):
        return '{}'.format(self.title)


MESSAGE_1 = 'MESSAGE_1'
MESSAGE_2 = 'MESSAGE_2'
MESSAGE_3 = 'MESSAGE_3'
MESSAGE_4 = 'MESSAGE_4'
MESSAGE_5 = 'MESSAGE_5'

MESSAGE_PROMO_1 = 'MESSAGE_PROMO_1'
MESSAGE_PROMO_2 = 'MESSAGE_PROMO_2'
MESSAGE_PROMO_3 = 'MESSAGE_PROMO_3'
MESSAGE_PROMO_4 = 'MESSAGE_PROMO_4'

MESSAGE_DUEL_1 = 'MESSAGE_DUEL_1'
MESSAGE_DUEL_2 = 'MESSAGE_DUEL_2'
MESSAGE_DUEL_3 = 'MESSAGE_DUEL_3'
MESSAGE_DUEL_4 = 'MESSAGE_DUEL_4'
MESSAGE_DUEL_5 = 'MESSAGE_DUEL_5'
MESSAGE_DUEL_6 = 'MESSAGE_DUEL_6'
MESSAGE_DUEL_7 = 'MESSAGE_DUEL_7'
MESSAGE_DUEL_8 = 'MESSAGE_DUEL_8'
MESSAGE_DUEL_9 = 'MESSAGE_DUEL_9'



MESSAGE_CYBER_CLUB_1 = 'MESSAGE_CYBER_CLUB_1'
MESSAGE_CYBER_CLUB_2 = 'MESSAGE_CYBER_CLUB_2'

MESSAGE_ABOUT_SERVICE = 'MESSAGE_ABOUT_SERVICE'

LAST_MESSAGE_HASH_SEARCH = 'LAST_MESSAGE_HASH_SEARCH'

MESSAGE_PERSONAL_AREA = 'MESSAGE_PERSONAL_AREA'
MESSAGE_PERSONAL_AREA_ABOUT = 'MESSAGE_PERSONAL_AREA_ABOUT'
MESSAGE_PERSONAL_AREA_MY_TOURNAMENTS_BTN = 'MESSAGE_PERSONAL_AREA_MY_TOURNAMENTS_BTN'
MESSAGE_PERSONAL_AREA_FAVORITES_BTN = 'MESSAGE_PERSONAL_AREA_FAVORITES_BTN'
MESSAGE_PERSONAL_AREA_NOTIFICATIONS_BTN = 'MESSAGE_PERSONAL_AREA_NOTIFICATIONS_BTN'
MESSAGE_PERSONAL_AREA_FRIENDS_BTN = 'MESSAGE_PERSONAL_AREA_FRIENDS_BTN'
MESSAGE_PERSONAL_AREA_MY_DUELS_BTN = 'MESSAGE_PERSONAL_AREA_MY_DUELS_BTN'
USER_IS_NOT_WRITE_ABOUT = 'USER_IS_NOT_WRITE_ABOUT'

MESSAGE_TOURNAMENT_1 = 'MESSAGE_TOURNAMENT_1'
MESSAGE_TOURNAMENT_2 = 'MESSAGE_TOURNAMENT_2'
MESSAGE_TOURNAMENT_3 = 'MESSAGE_TOURNAMENT_3'

MESSAGE_TYPES = (
    (MESSAGE_1, MESSAGE_1),
    (MESSAGE_2, MESSAGE_2),
    (MESSAGE_3, MESSAGE_3),
    (MESSAGE_4, MESSAGE_4),
    (MESSAGE_5, MESSAGE_5),
    (MESSAGE_PROMO_1, MESSAGE_PROMO_1),
    (MESSAGE_PROMO_2, MESSAGE_PROMO_2),
    (MESSAGE_PROMO_3, MESSAGE_PROMO_3),
    (MESSAGE_PROMO_4, MESSAGE_PROMO_4),
    (MESSAGE_CYBER_CLUB_1, MESSAGE_CYBER_CLUB_1),
    (MESSAGE_CYBER_CLUB_2, MESSAGE_CYBER_CLUB_2),
    (LAST_MESSAGE_HASH_SEARCH, LAST_MESSAGE_HASH_SEARCH),
    (MESSAGE_ABOUT_SERVICE, MESSAGE_ABOUT_SERVICE),
    (MESSAGE_DUEL_1, MESSAGE_DUEL_1),
    (MESSAGE_DUEL_2, MESSAGE_DUEL_2),
    (MESSAGE_DUEL_3, MESSAGE_DUEL_3),
    (MESSAGE_DUEL_4, MESSAGE_DUEL_4),
    (MESSAGE_DUEL_5, MESSAGE_DUEL_5),
    (MESSAGE_DUEL_6, MESSAGE_DUEL_6),
    (MESSAGE_DUEL_7, MESSAGE_DUEL_7),
    (MESSAGE_DUEL_8, MESSAGE_DUEL_8),
    (MESSAGE_DUEL_9, MESSAGE_DUEL_9),
    (MESSAGE_PERSONAL_AREA, MESSAGE_PERSONAL_AREA),
    (MESSAGE_PERSONAL_AREA_ABOUT, MESSAGE_PERSONAL_AREA_ABOUT),
    (MESSAGE_PERSONAL_AREA_MY_TOURNAMENTS_BTN, MESSAGE_PERSONAL_AREA_MY_TOURNAMENTS_BTN),
    (MESSAGE_PERSONAL_AREA_FAVORITES_BTN, MESSAGE_PERSONAL_AREA_FAVORITES_BTN),
    (MESSAGE_PERSONAL_AREA_NOTIFICATIONS_BTN, MESSAGE_PERSONAL_AREA_NOTIFICATIONS_BTN),
    (MESSAGE_PERSONAL_AREA_FRIENDS_BTN, MESSAGE_PERSONAL_AREA_FRIENDS_BTN),
    (MESSAGE_PERSONAL_AREA_MY_DUELS_BTN, MESSAGE_PERSONAL_AREA_MY_DUELS_BTN),
    (USER_IS_NOT_WRITE_ABOUT, USER_IS_NOT_WRITE_ABOUT),
    (MESSAGE_TOURNAMENT_1, MESSAGE_TOURNAMENT_1),
    (MESSAGE_TOURNAMENT_2, MESSAGE_TOURNAMENT_2),
    (MESSAGE_TOURNAMENT_3, MESSAGE_TOURNAMENT_3)
)

class Message(models.Model):
    message_type = models.CharField(max_length=255, choices=MESSAGE_TYPES)
    text = models.TextField()

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"
        db_table = 'messages'

    def __str__(self):
        return '{}'.format(self.message_type)


class MessageToTournament(models.Model):
    text = models.TextField()
    author = models.ForeignKey(User, related_name='message_tournament_author', on_delete=models.SET_NULL, null=True, blank=True)
    tournament = models.ForeignKey(Tournament, related_name='tournament_messages', on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        verbose_name = "Сообщение турнира"
        verbose_name_plural = "Сообщения турниров"
        db_table = 'message_to_tournaments'

    def __str__(self):
        return '{} {} {}'.format(self.text, self.author, self.tournament)


class MessageFromTournament(models.Model):
    text = models.TextField()
    tournament = models.ForeignKey(Tournament, related_name='from_tournament_messages', on_delete=models.SET_NULL, null=True, blank=True)
    chat_id = models.CharField(max_length=255)
    is_sent = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Сообщение из турнира"
        verbose_name_plural = "Сообщения из турниров"
        db_table = 'message_from_tournaments'

    def __str__(self):
        return '{} {} {} {}'.format(self.text, self.chat_id, self.tournament, self.is_sent)


class MessageFromTournament(models.Model):
    text = models.TextField()
    tournament = models.ForeignKey(Tournament, related_name='from_tournament_messages', on_delete=models.SET_NULL, null=True, blank=True)
    chat_id = models.CharField(max_length=255)
    is_sent = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Сообщение из турнира"
        verbose_name_plural = "Сообщения из турниров"
        db_table = 'message_from_tournaments'

    def __str__(self):
        return '{} {} {} {}'.format(self.text, self.chat_id, self.tournament, self.is_sent)


class NewTournamentMessage(models.Model):
    tournament = models.ForeignKey(Tournament, related_name='new_tournament_messages', on_delete=models.SET_NULL, null=True, blank=True)
    chat_id = models.CharField(max_length=255)
    is_sent = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Сообщение новый турнир"
        verbose_name_plural = "Сообщения новые трурниры"
        db_table = 'new_tournament_messages'

    def __str__(self):
        return '{} {} {}'.format(self.chat_id, self.tournament, self.is_sent)


class NewPromotionMessage(models.Model):
    promotion = models.ForeignKey(PromotionAndDiscount, related_name='new_promotion_messages', on_delete=models.SET_NULL, null=True, blank=True)
    chat_id = models.CharField(max_length=255)
    is_sent = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Сообщение новая акция"
        verbose_name_plural = "Сообщения новые акции"
        db_table = 'new_promotion_messages'

    def __str__(self):
        return '{} {} {}'.format(self.chat_id, self.promotion, self.is_sent)


class NewDuelMessage(models.Model):
    duel = models.ForeignKey(Duel, related_name='new_duel_messages', on_delete=models.SET_NULL, null=True, blank=True)
    chat_id = models.CharField(max_length=255)
    is_sent = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Сообщение новая дуэль"
        verbose_name_plural = "Сообщения новые дуэли"
        db_table = 'new_duel_messages'

    def __str__(self):
        return '{} {} {}'.format(self.chat_id, self.duel, self.is_sent)


EQUIPMENT_TYPE_MONITOR = 'MONITOR'
EQUIPMENT_TYPE_PC_HARDWEAR = 'PC_HARDWEAR'

EQUIPMENT_TYPES = (
    (EQUIPMENT_TYPE_MONITOR, 'Монитор'),
    (EQUIPMENT_TYPE_PC_HARDWEAR, 'Железо')
)


class Equipment(models.Model):
    text = models.TextField(verbose_name='Описание')
    equipment_type = models.CharField(max_length=255, choices=EQUIPMENT_TYPES, verbose_name='Тип оборудования')
    comp_club = models.ForeignKey(CompClub, related_name='comp_club_equipments', on_delete=models.CASCADE, verbose_name='Компьютерный клуб')

    class Meta:
        verbose_name = "Оборудование"
        verbose_name_plural = "Оборудование"
        db_table = 'equipments'

    def __str__(self):
        return '{}'.format(self.text)


HALL_BOOT_CAMP = 'HALL_BOOT_CAMP'
HALL_GENERAL = 'HALL_GENERAL'
HALL_PLOYKA = 'HALL_PLOYKA'


HALL_TYPES = (
    (HALL_BOOT_CAMP, 'Bootcamp'),
    (HALL_GENERAL, 'Общий'),
    (HALL_PLOYKA, 'Плойка')
)

class Hall(models.Model):
    title = models.CharField(max_length=255)
    comp_club = models.ForeignKey(CompClub, related_name='comp_club_halls', on_delete=models.CASCADE)
    hall_type = models.CharField(max_length=255, choices=HALL_TYPES)
    equipments = models.ManyToManyField(Equipment, related_name='equipments_halls')

    def get_prices(self):
        text = ''

        for price in self.hall_prices.all():
            if price.price_type == PRICE_TO_HOUR:
                text += 'Стоимость за час: {}\n'.format(price.amount)
            elif price.price_type == PRICE_TO_PERIOD:
                text += 'Стоимость: {} за период с {} до {}\n'.format(price.amount, price.start_time, price.end_time)
            elif price.price_type == PRICE_TO_PACKAGE:
                text += 'Стоимость: {} за пакет {} часов\n'.format(price.amount, price.hours)
        return text


    def get_equipments(self):
        text = ''
        for equipment in self.equipments.all():
            text += '{}\n'.format(equipment.text)
        return text

    @property
    def get_places(self):
        from itertools import zip_longest
        lst = self.playing_places.all().order_by('id')
        n = len(lst) // 12
        return list(x for x in zip_longest(*[iter(lst)] * n))

        place_list = []

        pp = []

        for p in self.playing_places.all():
            print(p)
            if len(pp) < 8:
                pp.append(p)
            else:
                place_list.append(pp)
                pp = []

        if len(pp) > 0:
            place_list.append(pp)
        print(pp)
        return place_list



    class Meta:
        verbose_name = 'Зал'
        verbose_name_plural = 'Залы'
        db_table = 'halls'

    def __str__(self):
        return '{}'.format(self.title)



MONDAY = 'MONDAY'
TUESDAY = 'TUESDAY'
WEDNESDAY = 'WEDNESDAY'
THURSDAY = 'THURSDAY'
FRIDAY = 'FRIDAY'
SATURDAY = 'SATURDAY'
SUNDAY = 'SUNDAY'


DAYS_OF_WEEK = (
    (MONDAY, 'Понедельник'),
    (TUESDAY, 'Вторник'),
    (WEDNESDAY, 'Среда'),
    (THURSDAY, 'Четверг'),
    (FRIDAY, 'Пятница'),
    (SATURDAY, 'Суббота'),
    (SUNDAY, 'Воскресенье'),
)

PRICE_TO_HOUR = 'price_to_hour'
PRICE_TO_PERIOD = 'price_to_period'
PRICE_TO_PACKAGE = 'price_to_package'

PRICE_TYPES = (
    (PRICE_TO_HOUR, 'Цена за час'),
    (PRICE_TO_PERIOD, 'Цена за период'),
    (PRICE_TO_PACKAGE, 'Цена за пакет'),
)

class Price(models.Model):
    day_of_week = models.CharField(max_length=255, choices=DAYS_OF_WEEK, verbose_name='День недели')
    start_time = models.TimeField(verbose_name='Начало')
    end_time = models.TimeField(verbose_name='Конец')
    amount = models.DecimalField(max_digits=11, decimal_places=2, verbose_name='Стоимость')
    hall = models.ForeignKey(Hall, related_name='hall_prices', on_delete=models.CASCADE, verbose_name='Зал')
    price_type = models.CharField(max_length=255, choices=PRICE_TYPES, default=PRICE_TO_HOUR)
    hours = models.PositiveIntegerField(default=1, verbose_name='Количество частов в пакете')

    def get_price_info(self):
        if self.price_type == PRICE_TO_HOUR:
            return 'Цена за час {}'.format(self.amount)
        if self.price_type == PRICE_TO_PACKAGE:
            return 'Цена за пакет {} колмчество часов {}'.format(self.amount, self.hours)
        if self.price_type == PRICE_TO_PERIOD:
            return 'Цена за период {} c {} до {}'.format(self.amount, self.start_time, self.end_time)



    class Meta:
        verbose_name = 'Тариф'
        verbose_name_plural = 'Тарифы'
        db_table = 'prices'

    def __str__(self):
        return '{}'.format(self.day_of_week)


class PlayingPlace(models.Model):
    title = models.CharField(max_length=255, verbose_name='Название', blank=True, null=True)
    hall = models.ForeignKey(Hall, related_name='playing_places', on_delete=models.CASCADE, verbose_name='Зал')
    equipments = models.ManyToManyField(Equipment, related_name='equipments_playing_places')
    in_work = models.BooleanField(default=True)

    @property
    def get_reserv_to_day(self):
        return self.reservation_playing_places.filter(reservation_date=datetime.now().date())

    class Meta:
        verbose_name = 'Игровое место'
        verbose_name_plural = 'Игровые места'
        db_table = 'playing_places'

    def __str__(self):
        return '{}'.format(self.title)


class Reservation(models.Model):
    playing_place = models.ForeignKey(PlayingPlace, related_name='reservation_playing_places',
                                      verbose_name='Игровое место', on_delete=models.SET_NULL, blank=True, null=True)
    chat_id = models.CharField(max_length=255)
    reservation_date = models.DateField(blank=True, null=True)
    reservation_time = models.TimeField(blank=True, null=True)
    reservation_end_time = models.TimeField(blank=True, null=True)
    is_booked_client = models.BooleanField(default=False, verbose_name='Забронировано клиентом')
    is_confirmed = models.BooleanField(default=False, verbose_name='Подтверждено админом')
    price = models.ForeignKey(Price, related_name='reservation_prices', on_delete=models.SET_NULL, blank=True, null=True)


    class Meta:
        verbose_name = 'Бронирование'
        verbose_name_plural = 'Бронирования'
        db_table = 'reservations'

    def __str__(self):
        return '{}'.format(self.chat_id)


