from django.contrib import admin

from app_comp_club import models


class CompClubAdmin(admin.ModelAdmin):
    model = models.CompClub

    list_display = ('title', 'club_id',)

admin.site.register(models.City)
admin.site.register(models.Area)
admin.site.register(models.Tag)

admin.site.register(models.Tournament)
admin.site.register(models.PromotionAndDiscount)
admin.site.register(models.FotoClub)
admin.site.register(models.Rating)

admin.site.register(models.TelegramProfile)
admin.site.register(models.CompClub, CompClubAdmin)
