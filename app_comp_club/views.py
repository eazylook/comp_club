from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
import json
import telebot

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from app_comp_club import actions
from app_comp_club import models


settings.BOT.set_webhook(url=settings.WEBHOOK_URL_BASE + settings.WEBHOOK_URL_PATH)

action_dict = {
    'start': actions.start,
    'select_city': actions.select_city,
    'dummy': actions.dummy,
    'find_by_areas': actions.find_by_areas,
    'find_by_tags': actions.find_by_tags,
    'find_by_tournaments': actions.find_by_tournaments,
    'find_by_discounts': actions.find_by_discounts,
    'select_area': actions.select_area,
    'select_tournament': actions.select_tournament,
    'next_foto': actions.next_foto,
    'back_foto': actions.back_foto,
    'select_tag': actions.select_tag,
    'is_shit': actions.is_shit,
    'is_good': actions.is_good,
    'is_shit_save_message': actions.is_shit_save_message,
    'is_good_save_message': actions.is_good_save_message,
    'actions': actions.c_promotiotions,
    'tournaments': actions.tournaments,
    'reviews': actions.reviews,
    'add_favorite': actions.add_favorite,
    'pa_client_start': actions.pa_client_start,
    'pa_club_start': actions.pa_club_start,
    'pac_favorites': actions.pac_favorites,
    'pac_mytournaments': actions.pac_mytournaments,
    'ccc_tour': actions.ccc_tour,
    'comp_club_card': actions.comp_club_card,
    'comp_club_active': actions.comp_club_active,
    'comp_club_save_tour': actions.comp_club_save_tour,
    'ccc_promotion': actions.ccc_promotion,
    'ccc_promotion_save': actions.ccc_promotion_save,
    'pac_dada': actions.pac_dada,
    'pac_about': actions.pac_about,
    'pac_about_save': actions.pac_about_save,
    'get_phone': actions.get_phone,
    'pa_selector': actions.pa_selector,
    'cclient_tour': actions.cclient_tour,
    'client_save_tour': actions.client_save_tour,
    'ct_delete': actions.ct_delete,
    'notification_promotion': actions.notification_promotion,
    'notification_tournament': actions.notification_tournament,
    'notification_duel': actions.notification_duel,
    'all_promotions': actions.all_promotions,
    'all_clubs': actions.all_clubs,
    'iamclub': actions.iamclub,
    'call_club': actions.call_club,
    'delete_promotion': actions.delete_promotion,
    'delete_tournament': actions.delete_tournament,
    'my_club': actions.my_club,
    'edit_desc_club': actions.edit_desc_club,
    'edit_desc_club_save': actions.edit_desc_club_save,
    'all_promotion': actions.all_promotion,
    'all_tournament': actions.all_tournament,
    'all_clubs_next': actions.all_clubs_next,
    'duels': actions.duels,
    'save_duel': actions.save_duel,
    'hash_search': actions.hash_search,
    'about_service': actions.about_service,
    'service_contacts': actions.service_contacts,
    'service_instruction': actions.service_instruction,
    'service_partners': actions.service_partners,
    'like_promotion': actions.like_promotion,
    'show_club': actions.show_club,
    'like_tournament': actions.like_tournament,
    'notification_panel': actions.notification_panel,
    'friends': actions.friends,
    'friend_save': actions.friend_save,
    'friend_confirmation': actions.friend_confirmation,
    'send_message_to_user': actions.send_message_to_user,
    'participate_tournament': actions.participate_tournament,
    'my_tournaments': actions.my_tournaments,
    'add_duel': actions.add_duel,
    'add_ordinary_duel': actions.add_ordinary_duel,
    'add_ordinary_duel_save': actions.add_ordinary_duel_save,
    'take_challenge': actions.take_challenge,
    'pac_myduels': actions.pac_myduels,
    'del_my_duel': actions.del_my_duel,
    'write_m_to_duel_author': actions.write_message_to_duel_author,
    'send_message_to_duel_author': actions.send_message_to_duel_author,
    'send_message_to_duel_author': actions.send_message_to_duel_author,
    'duel_participants': actions.duel_participants,
    'send_message_to_duel_user': actions.send_message_to_duel_user,
    'add_duel_withsearchteas': actions.add_duel_withsearchteas,
    'add_duel_withsearchteas_save': actions.add_duel_withsearchteas_save,
    'add_duel_withsearchteas_add_count': actions.add_duel_withsearchteas_add_count,
    'nick_name_save': actions.nick_name_save,
    'nick_name_saves': actions.nick_name_saves,
    'select_city_save': actions.select_city_save,
    'select_city_saves': actions.select_city_saves,
    'discord_save': actions.discord_save,
    'discord_saves': actions.discord_saves,
    'steam_save': actions.steam_save,
    'steam_saves': actions.steam_saves,
    'izmemenia': actions.izmemenia,
    'games_save': actions.games_save,
    'games_saves': actions.games_saves,
    'user_telegram_save': actions.user_telegram_save,
    'user_telegram_saves': actions.user_telegram_saves,
    'user_info': actions.user_info,
    'duel_write_message_all': actions.duel_write_message_all,
    'duel_send_message_all': actions.duel_send_message_all,
    'show_tournament_info': actions.show_tournament_info,
    'tournament_write_message_all': actions.tournament_write_message_all,
    'tournament_send_message_all': actions.tournament_send_message_all,
    'user_info_from_profile': actions.user_info_from_profile,
    'friend_del': actions.friend_del,
    'tours_clubs': actions.tours_clubs,
    'privat_tours': actions.privat_tours,
    'tours_listgames': actions.tours_listgames,
    'show_club_tournaments': actions.show_club_tournaments,
    'show_user_tournaments': actions.show_user_tournaments,
    'user_create_tournament': actions.user_create_tournament,
    'user_create_tournament_save': actions.user_create_tournament_save,
    'write_message_to': actions.write_message_to,
    'send_message_to': actions.send_message_to,
    'user_tourn_participants': actions.user_tourn_participants,
    'cancepart_tournament': actions.cancepart_tournament,
    'become_admin': actions.become_admin,
    'donate': actions.donate,
    'btn_about_s': actions.btn_about_s,
    'send_request': actions.send_request,
    'del_clubfavorite': actions.del_clubfavorite,
    'looking_for_clubs': actions.looking_for_clubs,
    'smart_search': actions.smart_search,
    'tours_clubs_next': actions.tours_clubs_next,
    'privat_tours_next': actions.privat_tours_next,
    'halls': actions.halls,
    'select_hall': actions.select_hall,
    'start_reservation': actions.start_reservation,
    'select_reservation_date': actions.select_reservation_date,
    'reservation_start_time': actions.reservation_start_time,
    'reservation_end_time': actions.reservation_end_time,
    'end_reservation': actions.end_reservation,
    'select_places': actions.select_places,
    'show_hall_places': actions.show_hall_places,
    'place_selected': actions.place_selected,
    'save_reservation': actions.save_reservation,
    'select_places_not_save_time': actions.select_places_not_save_time
}


@csrf_exempt
def action(request):
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)
    update = telebot.types.Update.de_json(body)
    settings.BOT.process_new_updates([update])

    @settings.BOT.message_handler(commands=['перезагрузить', 'reload'])
    def reload(message):
        work_action = action_dict.get('start')
        if work_action:
            work_action(chat_id=message.from_user.id, select_item_id=None, message=message)
        else:
            action_dict.get('dummy')(chat_id=message.from_user.id, select_item_id=None, message=message)

        return JsonResponse({})

    @settings.BOT.message_handler(commands=['города', 'city'])
    def city(message):
        work_action = action_dict.get('start')
        settings.BOT.send_message(message.from_user.id, 'Список городов 🏛 \n{}'.format('\n'.join(['#{}'.format(city.title) for city in models.City.objects.all()])))

        return JsonResponse({})


    @settings.BOT.message_handler(commands=['перезагрузить', 'reload'])
    def reload(message):
        work_action = action_dict.get('start')
        if work_action:
            work_action(chat_id=message.from_user.id, select_item_id=None, message=message)
        else:
            action_dict.get('dummy')(chat_id=message.from_user.id, select_item_id=None, message=message)

        return JsonResponse({})

    @settings.BOT.message_handler(commands=['моиданные', 'mydetails'])
    def mydetails(message):


        work_action = action_dict.get('pa_client_start')
        if work_action:
            work_action(chat_id=message.from_user.id, select_item_id=None, message=message)
        else:
            action_dict.get('dummy')(chat_id=message.from_user.id, select_item_id=None, message=message)

        return JsonResponse({})

    @settings.BOT.message_handler(commands=['моитурниры', 'mytournaments'])
    def mytournaments(message):

        work_action = action_dict.get('my_tournaments')
        if work_action:
            work_action(chat_id=message.from_user.id, select_item_id=None, message=message)
        else:
            action_dict.get('dummy')(chat_id=message.from_user.id, select_item_id=None, message=message)

        return JsonResponse({})

    @settings.BOT.message_handler(commands=['моидуэли', 'myduels'])
    def myduels(message):

        work_action = action_dict.get('pac_myduels')
        if work_action:
            work_action(chat_id=message.from_user.id, select_item_id=None, message=message)
        else:
            action_dict.get('dummy')(chat_id=message.from_user.id, select_item_id=None, message=message)

        return JsonResponse({})

    @settings.BOT.message_handler(commands=['профиль', 'profile'])
    def profile(message):

        work_action = action_dict.get('pac_about')
        if work_action:
            work_action(chat_id=message.from_user.id, select_item_id=None, message=message)
        else:
            action_dict.get('dummy')(chat_id=message.from_user.id, select_item_id=None, message=message)

        return JsonResponse({})

    @settings.BOT.message_handler(commands=['инструкция', 'instruction'])
    def instruction(message):

        work_action = action_dict.get('service_instruction')
        if work_action:
            work_action(chat_id=message.from_user.id, select_item_id=None, message=message)
        else:
            action_dict.get('dummy')(chat_id=message.from_user.id, select_item_id=None, message=message)

        return JsonResponse({})

    @settings.BOT.message_handler(commands=['уведомления', 'notifications'])
    def notifications(message):

        work_action = action_dict.get('notification_panel')
        if work_action:
            work_action(chat_id=message.from_user.id, select_item_id=None, message=message)
        else:
            action_dict.get('dummy')(chat_id=message.from_user.id, select_item_id=None, message=message)

        return JsonResponse({})

    @settings.BOT.message_handler(commands=['избранное', 'favotites'])
    def favotites(message):

        work_action = action_dict.get('pac_favorites')
        if work_action:
            work_action(chat_id=message.from_user.id, select_item_id=None, message=message)
        else:
            action_dict.get('dummy')(chat_id=message.from_user.id, select_item_id=None, message=message)

        return JsonResponse({})

    @settings.BOT.message_handler(commands=['buttons',])
    def favotites(message):

        bot_keyboard = telebot.types.InlineKeyboardMarkup(row_width=8)

        data_list = [
            ['1c', '1c', '1c', '1c'],
            ['1c', '1c', '1c', '1c'],
            ['1c', '1c', '1c', '1c'],
            ['1c', '1c', '1c', '1c'],
            ['1c', '1c', '1c', '1c'],
            ['1c', '1c', '1c', '1c'],
            ['1c', '1c', '1c', '1c'],
            ['1c', '1c', '1c', '1c'],
            ['1c', '1c', '1c', '1c'],
            ['1c', '1c', '1c', '1c'],
            ['1c', '1c', '1c', '1c'],
        ]

        for ll in data_list:
            btn_list = [
                telebot.types.InlineKeyboardButton(text=' ', callback_data='1c'),
                telebot.types.InlineKeyboardButton(text=' ', callback_data='1c'),
                telebot.types.InlineKeyboardButton(text=' ', callback_data='1c'),
                telebot.types.InlineKeyboardButton(text=' ', callback_data='1c'),
                telebot.types.InlineKeyboardButton(text=' ', callback_data='1c'),
                telebot.types.InlineKeyboardButton(text=' ', callback_data='1c'),
                telebot.types.InlineKeyboardButton(text=' ', callback_data='1c'),
                telebot.types.InlineKeyboardButton(text=' ', callback_data='1c'),
            ]
            bot_keyboard.add(*btn_list)

        settings.BOT.send_message(message.from_user.id, 'BTNS', reply_markup=bot_keyboard)


        return JsonResponse({})

    @settings.BOT.callback_query_handler(func=lambda message: True)
    def process_step(message):

        if message.data == '1c':
            chat_id = message.message.chat.id
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            telegram_profile.selected_sity = 'санктпетербург'
            telegram_profile.save()
            settings.BOT.answer_callback_query(callback_query_id=message.id, text="Ты выбрал Питер по умолчанию!", show_alert=True)
        elif message.data == '2c':
            chat_id = message.message.chat.id
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            telegram_profile.selected_sity = 'москва'
            telegram_profile.save()
            settings.BOT.answer_callback_query(callback_query_id=message.id, text="Ты выбрал Москву по умолчанию!", show_alert=True)
        elif message.data == '3c':
            chat_id = message.message.chat.id
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            telegram_profile.selected_sity = 'махачкала'
            telegram_profile.save()
            settings.BOT.answer_callback_query(callback_query_id=message.id, text="Ты выбрал махачкала по умолчанию!", show_alert=True)
        elif message.data == '4c':
            chat_id = message.message.chat.id
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            telegram_profile.selected_sity = 'краснодар'
            telegram_profile.save()
            settings.BOT.answer_callback_query(callback_query_id=message.id, text="Ты выбрал краснодар по умолчанию!", show_alert=True)
        elif message.data == '5c':
            chat_id = message.message.chat.id
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            telegram_profile.selected_sity = 'казань'
            telegram_profile.save()
            settings.BOT.answer_callback_query(callback_query_id=message.id, text="Ты выбрал казань по умолчанию!", show_alert=True)
        elif message.data == '6c':
            chat_id = message.message.chat.id
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            telegram_profile.selected_sity = 'пермь'
            telegram_profile.save()
            settings.BOT.answer_callback_query(callback_query_id=message.id, text="Ты выбрал пермь по умолчанию!", show_alert=True)
        elif message.data == '7c':
            chat_id = message.message.chat.id
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            telegram_profile.selected_sity = 'екатеринбург'
            telegram_profile.save()
            settings.BOT.answer_callback_query(callback_query_id=message.id, text="Ты выбрал екатеринбург по умолчанию!", show_alert=True)
        elif message.data == '8c':
            chat_id = message.message.chat.id
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            telegram_profile.selected_sity = 'ростовнадону'
            telegram_profile.save()
            settings.BOT.answer_callback_query(callback_query_id=message.id, text="Ты выбрал ростовнадону по умолчанию!", show_alert=True)
        elif message.data == '9c':
            chat_id = message.message.chat.id
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            telegram_profile.selected_sity = 'сочи'
            telegram_profile.save()
            settings.BOT.answer_callback_query(callback_query_id=message.id, text="Ты выбрал сочи по умолчанию!", show_alert=True)
        elif message.data == '10c':
            chat_id = message.message.chat.id
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            telegram_profile.selected_sity = 'владивосток'
            telegram_profile.save()
            settings.BOT.answer_callback_query(callback_query_id=message.id, text="Ты выбрал владивосток по умолчанию!", show_alert=True)
        elif message.data == '11c':
            chat_id = message.message.chat.id
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            telegram_profile.selected_sity = 'нижнийновгород'
            telegram_profile.save()
            settings.BOT.answer_callback_query(callback_query_id=message.id, text="Ты выбрал нижнийновгород по умолчанию!", show_alert=True)
        elif message.data == '12c':
            chat_id = message.message.chat.id
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            telegram_profile.selected_sity = 'тюмень'
            telegram_profile.save()
            settings.BOT.answer_callback_query(callback_query_id=message.id, text="Ты выбрал тюмень по умолчанию!", show_alert=True)
        elif message.data == '13c':
            chat_id = message.message.chat.id
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            telegram_profile.selected_sity = 'новосибирск'
            telegram_profile.save()
            settings.BOT.answer_callback_query(callback_query_id=message.id, text="Ты выбрал новосибирск по умолчанию!", show_alert=True)
        elif message.data == '14c':
            chat_id = message.message.chat.id
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            telegram_profile.selected_sity = 'тула'
            telegram_profile.save()
            settings.BOT.answer_callback_query(callback_query_id=message.id, text="Ты выбрал тула по умолчанию!", show_alert=True)
        elif message.data == '15c':
            chat_id = message.message.chat.id
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            telegram_profile.selected_sity = 'томск'
            telegram_profile.save()
            settings.BOT.answer_callback_query(callback_query_id=message.id, text="Ты выбрал томск по умолчанию!", show_alert=True)
        elif message.data == '16c':
            chat_id = message.message.chat.id
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            telegram_profile.selected_sity = 'челябинск'
            telegram_profile.save()
            settings.BOT.answer_callback_query(callback_query_id=message.id, text="Ты выбрал челябинск по умолчанию!", show_alert=True)
        elif message.data == '17c':
            chat_id = message.message.chat.id
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            telegram_profile.selected_sity = 'уфа'
            telegram_profile.save()
            settings.BOT.answer_callback_query(callback_query_id=message.id, text="Ты выбрал уфа по умолчанию!", show_alert=True)
        elif message.data == '18c':
            chat_id = message.message.chat.id
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            telegram_profile.selected_sity = 'минск'
            telegram_profile.save()
            settings.BOT.answer_callback_query(callback_query_id=message.id, text="Ты выбрал минск по умолчанию!", show_alert=True)

        else:
            action, return_value, *additional_arrgs = message.data.split(':')

            print(action)

            if len(message.data.split(':')) == 3:
                return_value = return_value + ':' + additional_arrgs[0]

            work_action = action_dict.get(action)

            if work_action:
                work_action(chat_id=message.from_user.id, select_item_id=return_value, message=message)
            else:
                action_dict.get('dummy')(chat_id=message.from_user.id, select_item_id=return_value, message=message)

    @settings.BOT.message_handler(commands=['start', 'начать'])
    def send_welcome(message):
        chat_id = message.chat.id

        reply_keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True)

        reply_keyboard.add(*['Профиль 🗿', 'Акции 🎉'])
        reply_keyboard.add(*['Турниры 🕹', 'О сервисе 🤖'])
        reply_keyboard.add(*['Поиск Cyber-клуба🧐',])

        bot_keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)

        data_list = [
            [
                '1c'
                '2c'
                '3c'
                '4c'
                '5c'
                '6c'
                '7c'
                '8c'
                '9c'
                '10c'
                '11c'
                '12c'
                '13c'
                '14c'
                '15c'
                '16c'
                '17c'
                '18c'
             ],

        ]

        for ll in data_list:
            btn_list = [
                telebot.types.InlineKeyboardButton(text='Санкт-Петербург', callback_data='1c'),
                telebot.types.InlineKeyboardButton(text='Москва', callback_data='2c'),
                telebot.types.InlineKeyboardButton(text='Махачкала', callback_data='3c'),
                telebot.types.InlineKeyboardButton(text='Краснодар', callback_data='4c'),
                telebot.types.InlineKeyboardButton(text='Казань', callback_data='5c'),
                telebot.types.InlineKeyboardButton(text='Пермь', callback_data='6c'),
                telebot.types.InlineKeyboardButton(text='Екатеринбург', callback_data='7c'),
                telebot.types.InlineKeyboardButton(text='Ростов-На-Дону', callback_data='8c'),
                telebot.types.InlineKeyboardButton(text='Сочи', callback_data='9c'),
                telebot.types.InlineKeyboardButton(text='Владивосток', callback_data='10c'),
                telebot.types.InlineKeyboardButton(text='Нижний-Новгород', callback_data='11c'),
                telebot.types.InlineKeyboardButton(text='Тюмень', callback_data='12c'),
                telebot.types.InlineKeyboardButton(text='Новосибирск', callback_data='13c'),
                telebot.types.InlineKeyboardButton(text='Тула', callback_data='14c'),
                telebot.types.InlineKeyboardButton(text='Томск', callback_data='15c'),
                telebot.types.InlineKeyboardButton(text='Челябинск', callback_data='16c'),
                telebot.types.InlineKeyboardButton(text='Уфа', callback_data='17c'),
                telebot.types.InlineKeyboardButton(text='Минск', callback_data='18c'),
            ]
            bot_keyboard.add(*btn_list)

        settings.BOT.send_message(message.from_user.id, 'Привет! Для начала выбери свой город 🏛', reply_markup=bot_keyboard)
        settings.BOT.send_message(message.from_user.id, 'Выбери свой город сверху! Я его сделаю по умолчанию',reply_markup=reply_keyboard)

        try:
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            if telegram_profile.selected_sity is None:
                telegram_profile.selected_sity = 'москва'
                telegram_profile.save()
        except ObjectDoesNotExist:
            telegram_profile = models.TelegramProfile.objects.create(chat_id=chat_id)
            if telegram_profile.selected_sity is None:
                telegram_profile.selected_sity = 'москва'
                telegram_profile.save()


    @settings.BOT.message_handler(func=lambda message: True, content_types=['photo',])
    def add_photo(message):
        telegram_profile = models.TelegramProfile.objects.get(chat_id=message.from_user.id)

        work_action = None

        if telegram_profile.current_action == 'ccc_promotion':
            work_action = action_dict.get('ccc_promotion_save')

        if telegram_profile.current_action == 'ccc_tour':
            work_action = action_dict.get('comp_club_save_tour')

        if telegram_profile.current_action == 'send_request':
            work_action = action_dict.get('user_create_tournament_save')


        if work_action:
            work_action(chat_id=message.from_user.id, select_item_id=None, message=message)
        else:
            action_dict.get('dummy')(chat_id=message.from_user.id, select_item_id=None, message=message)

    @settings.BOT.message_handler(func=lambda message: True, content_types=['text',])
    def add_answer(message):

        telegram_profile = models.TelegramProfile.objects.get(chat_id=message.from_user.id)

        work_action = None

        if telegram_profile.current_action == 'is_shit':
            work_action = action_dict.get('is_shit_save_message')

        if telegram_profile.current_action == 'is_good':
            work_action = action_dict.get('is_good_save_message')

        if telegram_profile.current_action == 'iamclub':
            work_action = action_dict.get('comp_club_active')

        if telegram_profile.current_action == 'nick_name_saves':
            work_action = action_dict.get('nick_name_save')

        if telegram_profile.current_action == 'steam_saves':
            work_action = action_dict.get('steam_save')

        if telegram_profile.current_action == 'discord_saves':
            work_action = action_dict.get('discord_save')

        if telegram_profile.current_action == 'games_saves':
            work_action = action_dict.get('games_save')

        if telegram_profile.current_action == 'user_telegram_saves':
            work_action = action_dict.get('user_telegram_save')

        if telegram_profile.current_action == 'select_city_saves':
            work_action = action_dict.get('select_city_save')

        if telegram_profile.current_action == 'cclient_tour':
            work_action = action_dict.get('client_save_tour')

        if telegram_profile.current_action == 'edit_desc_club':
            work_action = action_dict.get('edit_desc_club_save')

        if telegram_profile.current_action == 'duels':
            work_action = action_dict.get('save_duel')

        if telegram_profile.current_action == 'friends':
            work_action = action_dict.get('friend_save')

        if telegram_profile.current_action == 'add_ordinary_duel':
            work_action = action_dict.get('add_ordinary_duel_save')

        if telegram_profile.current_action == 'write_message_to_duel_author':
            work_action = action_dict.get('send_message_to_duel_user')

        if telegram_profile.current_action == 'add_duel_withsearchteas':
            work_action = action_dict.get('add_duel_withsearchteas_save')

        if telegram_profile.current_action == 'add_duel_withsearchteas_save':
            work_action = action_dict.get('add_duel_withsearchteas_add_count')

        if telegram_profile.current_action == 'duel_write_message_all':
            work_action = action_dict.get('duel_send_message_all')

        if telegram_profile.current_action == 'tournament_write_message_all':
            work_action = action_dict.get('tournament_send_message_all')

        if telegram_profile.current_action == 'write_message_to':
            work_action = action_dict.get('send_message_to')

        if message.text == 'Начать 🚀':
            work_action = action_dict.get('start')

        if message.text == 'Личный кабинет🧑🏼‍💻':
            work_action = action_dict.get('pa_selector')

        if message.text == 'Создать Турнир🕹':
            work_action = action_dict.get('cclient_tour')

        if message.text == 'Акции 🎉':
            work_action = action_dict.get('all_promotions')

        if message.text == 'Поиск Cyber-клуба🧐':
            work_action = action_dict.get('looking_for_clubs')

        if message.text == 'iamclub':
            work_action = action_dict.get('iamclub')

        if message.text == 'Турниры 🕹':
            work_action = action_dict.get('all_tournament')

        if message.text == 'Акции':
            work_action = action_dict.get('all_promotion')

        if message.text[:1] == '#':
            work_action = action_dict.get('hash_search')

        if message.text == 'О сервисе 🤖':
            work_action = action_dict.get('about_service')

        if message.text[:10] == 'Профиль 🗿':
            work_action = action_dict.get('pa_client_start')

        if message.text[:1] == '/':
            work_action = action_dict.get('send_message_to_user')

        if message.text[:1] == '&':
            work_action = action_dict.get('become_admin')


        if work_action:
            work_action(chat_id=message.from_user.id, select_item_id=None, message=message)
        else:
            action_dict.get('dummy')(chat_id=message.from_user.id, select_item_id=None, message=message)


    return JsonResponse({})

