import typing
import ast

import telebot

from itertools import zip_longest
from app_comp_club import models


def chunks(lst, count):
    n = len(lst) // count
    return list(x for x in zip_longest(*[iter(lst)] * n))


def chuck_new_count(lst, count):
    new_list = []
    item_count = len(lst)
    last_item_number = 0
    print(item_count)

    parts = round(item_count / count)
    print(parts)

    for i in range(0, parts):
        new_list.append(lst[last_item_number:(last_item_number + count)])
        last_item_number = last_item_number + count
    return new_list


comp_club_questionnaire = '{title} \n {description}'
promotion_scheme = '{title}'
tournament_scheme = '{title}'
tournament_scheme_user = '№{id}\n{title}\nопубликовано {create_date}'
rating_scheme = '{text}'
duel_scheme = '{get_type} №{id}\n{title}\n 🧨{get_hours} часов до завершения\n{get_create_date}'

message_schemes = dict(comp_club_questionnaire=comp_club_questionnaire, promotion_scheme=promotion_scheme,
                       tournament_scheme=tournament_scheme, rating_scheme=rating_scheme, duel_scheme=duel_scheme,
                       tournament_scheme_user=tournament_scheme_user)


def keyboard_creator_from_queryset(callback_name: str, queryset, value_name: str) -> telebot.types.InlineKeyboardMarkup:
    bot_keyboard = telebot.types.InlineKeyboardMarkup()

    for item in queryset:
        bot_keyboard.add(telebot.types.InlineKeyboardButton(text=getattr(item, value_name),
                                                            callback_data='{}:{}'.format(callback_name, item.id)))

    return bot_keyboard


def keyboard_creator_from_data_list(data_list: typing.List[typing.List[str]], count=None, oneline=None, new_count=None) -> telebot.types.InlineKeyboardMarkup:
    bot_keyboard = telebot.types.InlineKeyboardMarkup()

    if new_count:
        lst = chuck_new_count(data_list, new_count)
        for ll in lst:
            btn_list = [telebot.types.InlineKeyboardButton(text=btm_txt,
                                                           callback_data='{}:{}'.format(callback_name, callback_name))
                        for btm_txt, callback_name in ll]
            bot_keyboard.add(*btn_list)
        return bot_keyboard


    if count:
        for ll in chunks(data_list, count):
            btn_list = [telebot.types.InlineKeyboardButton(text=btm_txt,
                                                            callback_data='{}:{}'.format(callback_name, callback_name)) for btm_txt, callback_name in ll]
            bot_keyboard.add(*btn_list)
    else:
        if oneline:
            btn_list = [telebot.types.InlineKeyboardButton(text=btm_txt,
                                                           callback_data='{}:{}'.format(callback_name, callback_name))
                        for btm_txt, callback_name in data_list]
            bot_keyboard.add(*btn_list)

        else:
            for btm_txt, callback_name in data_list:
                bot_keyboard.add(telebot.types.InlineKeyboardButton(text=btm_txt,
                                                            callback_data='{}:{}'.format(callback_name, callback_name)))

    return bot_keyboard


def keyboard_creator_to_promotion(data_list: typing.List[typing.List[str]]) -> telebot.types.InlineKeyboardMarkup:
    bot_keyboard = telebot.types.InlineKeyboardMarkup()

    btn_txt, callback_data = data_list
    bot_keyboard.add(telebot.types.InlineKeyboardButton(text=btn_txt,
                                                            callback_data=callback_data))

    return bot_keyboard


def keyboard_creator_from_data_list_float(data_list: typing.List[typing.List[str]]) -> telebot.types.InlineKeyboardMarkup:
    bot_keyboard = telebot.types.InlineKeyboardMarkup()

    for ll in data_list:
        btn_list = [telebot.types.InlineKeyboardButton(text=btm_txt,
                                                       callback_data='{}:{}'.format(callback_name, callback_name)) for
                    btm_txt, callback_name in ll]
        bot_keyboard.add(*btn_list)

    return bot_keyboard




def message_creator(item, value_keys, message_scheme: str) -> str:
    text_data = dict()

    for k in value_keys:
        text_data.update({k: getattr(item, k)})

    text = message_schemes.get(message_scheme).format(**text_data)
    return text


def get_comp_club_questionnaire_buttons(club, foto_number, float_list=False):
    if float_list:
        return [
                [['Акции 🎉 {}'.format(club.promotions_and_discounts.count()), 'actions:{}'.format(club.id)],
                ['Турниры 🕹 {}'.format(club.tournaments.count()), 'tournaments:{}'.format(club.id)],],
                [['Дуэли ⚔️ {}'.format(club.cc_duels.count()), 'duels:{}'.format(club.id)],['Подписаться ⭐️', 'add_favorite:{}'.format(club.id)],],
        ]
    else:
        return [
            ['⭐️', 'add_favorite:{}'.format(club.id)],
            ['Акции 🎉 {}'.format(club.promotions_and_discounts.count()), 'actions:{}'.format(club.id)],
            ['Турниры 🕹', 'tournaments:{}'.format(club.id)]
        ]

def get_current_profile(telegram_id):
    return models.TelegramProfile.objects.get(chat_id=telegram_id)

def get_last_values(value: str):
    return ast.literal_eval(value)


def call_back_buttons_creator(data_list):
    bot_keyboard = telebot.types.InlineKeyboardMarkup()

    for btm_txt, callback_name, callback_value in data_list:
        bot_keyboard.add(telebot.types.InlineKeyboardButton(text=btm_txt,
                                                            callback_data='{}:{}'.format(callback_name, callback_value)))

    return bot_keyboard


def make_user_name(chat):
    user_name_to_btn = None

    if chat.first_name:
        user_name_to_btn = chat.first_name

    if user_name_to_btn is None and chat.last_name:
        user_name_to_btn = chat.last_name

    if user_name_to_btn is None and chat.username:
        user_name_to_btn = chat.username

    if user_name_to_btn is None:
        user_name_to_btn = chat.id

    return user_name_to_btn


def make_user_info(telegram_profile):
    message = 'Ник: {}\n👀 Discord/ TeamSpeak: {}\n🌐 Steam/ Faceit: {}\n🏛 город: {}\n👾 игры: {}\n@ telegram: {}\nid игрока 🦸🏼‍♂️️: {}\n\n'.format(
        telegram_profile.nick_name if telegram_profile.nick_name else 'Нет',
        telegram_profile.discord if telegram_profile.discord else 'Нет',
        telegram_profile.steam if telegram_profile.steam else 'Нет',
        telegram_profile.selected_sity if telegram_profile.selected_sity else 'Нет',
        telegram_profile.games if telegram_profile.games else 'Нет',
        telegram_profile.telegram_nick if telegram_profile.telegram_nick else 'Нет',
        telegram_profile.chat_id
    )
    return message
