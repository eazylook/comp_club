from django.core.management.base import BaseCommand, CommandError

from django.conf import settings
from app_comp_club import models
from telebot.apihelper import ApiException
from app_comp_club import helpers
from app_comp_club import work_treads


class Command(BaseCommand):
    args = '<poll_id poll_id ...>'
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        messages = []

        for message in models.NewTournamentMessage.objects.filter(is_sent=False):
            file = open(message.tournament.img.path, 'rb')

            data_list = [
                ['Показать клуб', 'show_club:{}'.format(message.tournament.comp_club.id)],
                # ['❤ ({})'.format(tournament.likes), 'like_tournament:{}'.format(tournament.id)],
                ['Участвовать 💯', 'participate_tournament:{}'.format(message.tournament.id)]
            ]
            reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)

            messages.append(
                [helpers.message_creator(message.tournament, ['title', ], 'promotion_scheme'), file, reply_markup])

            message.is_sent = True
            message.save()


        send_messages_thread = work_treads.SendPhotoMessagesThread(message.chat_id, messages, after_thred=None)
        send_messages_thread.start()


