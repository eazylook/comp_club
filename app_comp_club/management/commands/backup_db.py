from django.core.management.base import BaseCommand, CommandError
import subprocess

from django.conf import settings

class Command(BaseCommand):
    args = '<poll_id poll_id ...>'
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        #subprocess.call('PGPASSWORD="1qazxsw2" pg_dump -c -h 127.0.0.1 -U toys_tele_bot_user toys_tele_bot_db > ./dump.sql', shell=True)

        db = settings.DATABASES.get('default').get('NAME')
        user = settings.DATABASES.get('default').get('USER')
        host = settings.DATABASES.get('default').get('HOST')
        password = settings.DATABASES.get('default').get('PASSWORD')

        backup_db_request = 'PGPASSWORD="{}" pg_dump -c -h {} -U {} {} > ./dump.sql'.format(password, host, user, db)

        subprocess.call(backup_db_request, shell=True)
        print(settings.BASE_DIR)

        f = open('/home/comp_club/dump.sql', 'r')
        print(f)
        #for admin in ['542812109', '919922402']:
        #    settings.BOT.send_document(admin, f)

