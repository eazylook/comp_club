from django.core.management.base import BaseCommand, CommandError

from django.conf import settings
from app_comp_club import models
from telebot.apihelper import ApiException
from app_comp_club import helpers
from app_comp_club import work_treads


class Command(BaseCommand):
    args = '<poll_id poll_id ...>'
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        messages = []

        for message in models.NewDuelMessage.objects.filter(is_sent=False):
            keyboard = helpers.call_back_buttons_creator([['Удалить', 'ct_delete', message.duel.id]])

            duel = message.duel

            duel_participants = models.MyDuel.objects.filter(duel_id=duel.id).count()

            if duel.number_participants and duel_participants >= duel.number_participants:
                continue

            data_list = [['{}'.format('Присоеденится {}/{}'.format(duel_participants,
                                                                   duel.number_participants) if duel.number_participants else 'Принять вызов ⚔️'),
                          'take_challenge:{}'.format(duel.id)]]
            reply_markup = helpers.keyboard_creator_from_data_list(data_list)

            messages.append(
                [helpers.message_creator(duel, ('id', 'title', 'get_hours', 'create_date', 'get_type'), 'duel_scheme'),
                 reply_markup])

            send_messages_thread = work_treads.SendMessagesThread(message.chat_id, messages)
            send_messages_thread.start()
