from django.core.management.base import BaseCommand, CommandError

from django.conf import settings
from app_comp_club import models
from telebot.apihelper import ApiException


class Command(BaseCommand):
    args = '<poll_id poll_id ...>'
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        for message in models.MessageFromTournament.objects.filter(is_sent=False):
            try:
                settings.BOT.send_message(message.chat_id, 'В турнире {} новое сообщение\n{}'.format(message.tournament.title, message.text))
            except ApiException:
                settings.BOT.send_message(message.chat_id,
                                          'В турнире {} новое сообщение\n{}'.format(message.tournament.title,
                                                                                    message.text))
            message.is_sent = True
            message.save()
