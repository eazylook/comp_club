from threading import Thread
import typing
import time

from telebot.apihelper import ApiException


from django.conf import settings


import telebot

class SendMessagesThread(Thread):
    def __init__(self, chat_id: str, messages, sleep=None, after_thred = None, parse_mode=None):
        Thread.__init__(self)
        self.chat_id = chat_id
        self.messages = messages
        self.sleep = sleep
        self.after_thred = after_thred
        self.parse_mode = "Markdown" if parse_mode is None else parse_mode

    def after_send(self):
        if self.after_thred:
            self.after_thred.run()

    def run(self):
        if self.sleep:
            time.sleep(int(self.sleep))

        for message, reply_markup in self.messages:

            if reply_markup:
                if self.parse_mode:
                    settings.BOT.send_message(self.chat_id, message, reply_markup=reply_markup, parse_mode=self.parse_mode)
                else:
                    settings.BOT.send_message(self.chat_id, message, reply_markup=reply_markup)
            else:
                if self.parse_mode:
                    settings.BOT.send_message(self.chat_id, message, parse_mode=self.parse_mode)
                else:
                    settings.BOT.send_message(self.chat_id, message)

        self.after_send()


class SendPhotoMessagesThread(SendMessagesThread):

    def run(self):
        for message, file, reply_markup in self.messages:

            try:
                if reply_markup:
                    settings.BOT.send_photo(self.chat_id, file, caption=message, reply_markup=reply_markup, parse_mode=self.parse_mode)
                else:
                    settings.BOT.send_photo(self.chat_id, file, caption=message, parse_mode=self.parse_mode)
            except ApiException as e:
                settings.BOT.send_message(self.chat_id, '{} {}'.format(message, e))

        self.after_send()
