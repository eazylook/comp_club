from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.conf import settings
import operator
from django.db.models import Q
from functools import reduce
import typing
import ast
import datetime
from django.utils import timezone
from telebot.apihelper import ApiException

from django.core.files import File
import requests
import telebot
from telebot.types import InputMediaPhoto

from app_comp_club import models
from app_comp_club import helpers
from app_comp_club import work_treads
import time
from django.utils.crypto import get_random_string
from datetime import timedelta



def current_action_save(function_to_decorate):
    def the_wrapper_around_the_original_function(*args, **kwargs):
        function_to_decorate(*args, **kwargs)
        print('SAVE CURRENT ACTION', function_to_decorate.__name__)
        print(kwargs)
        print('SAVE CURRENT ACTION', function_to_decorate.__name__)
        telegram_profile = models.TelegramProfile.objects.get(chat_id=kwargs.get('chat_id'))
        telegram_profile.current_action = function_to_decorate.__name__

        if kwargs.get('select_item_id') is not None:
            telegram_profile.last_select_values = str(dict(select_item_id=kwargs.get('select_item_id')))
        telegram_profile.save()
    return the_wrapper_around_the_original_function


@current_action_save
def start(chat_id=None, select_item_id=None, message=None):
    reply_markup = helpers.keyboard_creator_from_queryset('select_city', models.City.objects.all(), 'title')

    try:
        telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
        if telegram_profile.selected_sity is None:
            telegram_profile.selected_sity = 'москва'
            telegram_profile.save()
        settings.BOT.send_message(chat_id, 'Ваш город👇🏻', reply_markup=reply_markup)
    except ObjectDoesNotExist:
        telegram_profile = models.TelegramProfile.objects.create(chat_id=chat_id)
        if telegram_profile.selected_sity is None:
            telegram_profile.selected_sity = 'москва'
            telegram_profile.save()
        settings.BOT.send_message(chat_id, 'Ваш город👇🏻', reply_markup=reply_markup)


@current_action_save
def select_city(chat_id=None, select_item_id=None, message=None):
    data_list = [['Району📍', 'find_by_areas'], ['Тегам💡', 'find_by_tags'],
                 ['Акциям и скидкам🎉', 'find_by_discounts'], ['Турнирам🕹', 'find_by_tournaments']]
    reply_markup = helpers.keyboard_creator_from_data_list(data_list)
    settings.BOT.send_message(chat_id, 'Поиск клуба по:', reply_markup=reply_markup)


@current_action_save
def find_by_areas(chat_id=None, select_item_id=None, message=None):
    reply_markup = helpers.keyboard_creator_from_queryset('select_area', models.Area.objects.all(), 'title')
    settings.BOT.send_message(chat_id, 'Выбирете район:', reply_markup=reply_markup)


@current_action_save
def find_by_tags(chat_id=None, select_item_id=None, message=None):
    reply_markup = helpers.keyboard_creator_from_queryset('select_tag', models.Tag.objects.all(), 'title')
    settings.BOT.send_message(chat_id, 'Выберете тег:', reply_markup=reply_markup)


@current_action_save
def find_by_tournaments(chat_id=None, select_item_id=None, message=None):
    messages = []

    for comp_club in models.CompClub.objects.filter(tournaments__isnull=False):
        buttons = helpers.get_comp_club_questionnaire_buttons(comp_club, 0)

        keyboard = helpers.keyboard_creator_from_data_list(buttons, 3)

        file = open(comp_club.club_fotos.all().first().img.path, 'rb')
        messages.append(
            [helpers.message_creator(comp_club, ['title', 'price', 'area'], 'comp_club_questionnaire'), file,
             keyboard])

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages)
    send_messages_thread.start()

    #reply_markup = helpers.keyboard_creator_from_queryset('select_tournament', models.Tournament.objects.all(), 'title')
    #settings.BOT.send_message(chat_id, 'Выберете клуб:', reply_markup=reply_markup)


@current_action_save
def find_by_discounts(chat_id=None, select_item_id=None, message=None):
    messages = []

    for comp_club in models.CompClub.objects.filter(promotions_and_discounts__isnull=False):
        buttons = helpers.get_comp_club_questionnaire_buttons(comp_club, 0)

        keyboard = helpers.keyboard_creator_from_data_list(buttons, 3)

        file = open(comp_club.club_fotos.all().first().img.path, 'rb')
        messages.append(
            [helpers.message_creator(comp_club, ['title', 'description',], 'comp_club_questionnaire'), file,
             keyboard])

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages)
    send_messages_thread.start()


@current_action_save
def select_area(chat_id=None, select_item_id=None, message=None):

    messages = []

    for comp_club in models.CompClub.objects.filter(area_id=select_item_id):
        buttons = helpers.get_comp_club_questionnaire_buttons(comp_club, 0)

        keyboard = helpers.keyboard_creator_from_data_list(buttons, 3)

        file = open(comp_club.club_fotos.all().first().img.path, 'rb')
        messages.append([helpers.message_creator(comp_club, ['title', 'description',], 'comp_club_questionnaire'), file, keyboard])

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages)
    send_messages_thread.start()


@current_action_save
def select_tournament(chat_id=None, select_item_id=None, message=None):

    messages = []

    for tournament in models.Tournament.objects.filter(comp_club__isnull=False):
        buttons = helpers.get_comp_club_questionnaire_buttons(tournament.comp_club, 0,)

        keyboard = helpers.keyboard_creator_from_data_list(buttons, 3)

        file = open(tournament.comp_club.club_fotos.all().first().img.path, 'rb')
        messages.append([helpers.message_creator(tournament.comp_club, ['title', 'description',], 'comp_club_questionnaire'), file, keyboard])

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages)
    send_messages_thread.start()


@current_action_save
def next_foto(chat_id=None, select_item_id=None, message=None):

    messages = []

    foto_number, club_id = select_item_id.split('-')


    comp_club = models.CompClub.objects.get(id=club_id)

    if  (int(foto_number)+1) > (comp_club.club_fotos.count()-1):
        next_foto_number = int(foto_number)
    else:
        next_foto_number = int(foto_number) + 1


    file = open(comp_club.club_fotos.all()[next_foto_number].img.path, 'rb')

    buttons = helpers.get_comp_club_questionnaire_buttons(comp_club, next_foto_number, float_list=True)

    keyboard = helpers.keyboard_creator_from_data_list_float(buttons)
    club_url = 'https://telegra.ph/'

    if comp_club.club_url:
        club_url = comp_club.club_url

    btn_list = [telebot.types.InlineKeyboardButton(text='Фото 📷/ Тарифы 💰/ Железо 🖥',
                                                   url=club_url)]
    keyboard.add(*btn_list)
    btn_list = [
        telebot.types.InlineKeyboardButton(text='Забронировать 🗿', url=club_url),
    ]
    keyboard.add(*btn_list)

    #messages.append(
    #    [helpers.message_creator(comp_club, ['title', 'description',], 'comp_club_questionnaire'), file,
    #     keyboard])

    edit_text_message = helpers.message_creator(comp_club, ['title', 'description',], 'comp_club_questionnaire')


    msg = settings.BOT.edit_message_media(InputMediaPhoto(file), chat_id=chat_id, message_id=message.message.message_id,
                                          reply_markup=keyboard)
    edit_message_id = msg.message_id

    msg = settings.BOT.edit_message_caption(edit_text_message, chat_id=chat_id, message_id=edit_message_id, reply_markup=keyboard)

    #send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages)
    #send_messages_thread.start()


@current_action_save
def back_foto(chat_id=None, select_item_id=None, message=None):

    messages = []

    foto_number, club_id = select_item_id.split('-')


    comp_club = models.CompClub.objects.get(id=club_id)

    if (int(foto_number)-1) < 0:
        previos_foto_number = int(foto_number)
    else:
        previos_foto_number = int(foto_number) - 1

    file = open(comp_club.club_fotos.all()[previos_foto_number].img.path, 'rb')

    buttons = helpers.get_comp_club_questionnaire_buttons(comp_club, previos_foto_number, float_list=True)

    keyboard = helpers.keyboard_creator_from_data_list_float(buttons)

    club_url = 'https://telegra.ph/'

    if comp_club.club_url:
        club_url = comp_club.club_url

    btn_list = [telebot.types.InlineKeyboardButton(text='Фото 📷/ Тарифы 💰/ Железо 🖥',
                                                   url=club_url)]
    keyboard.add(*btn_list)
    btn_list = [
        telebot.types.InlineKeyboardButton(text='Забронировать 🗿', url=club_url),
    ]
    keyboard.add(*btn_list)

    edit_text_message = helpers.message_creator(comp_club, ['title', 'description', ], 'comp_club_questionnaire')

    msg = settings.BOT.edit_message_media(InputMediaPhoto(file), chat_id=chat_id, message_id=message.message.message_id,
                                          reply_markup=keyboard)
    edit_message_id = msg.message_id

    msg = settings.BOT.edit_message_caption(edit_text_message, chat_id=chat_id, message_id=edit_message_id,
                                            reply_markup=keyboard)

    #messages.append(
    #    [helpers.message_creator(comp_club, ['title', 'description',], 'comp_club_questionnaire'), file,
    #     keyboard])

    #send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages)
    #send_messages_thread.start()


@current_action_save
def dummy(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id, 'В разработке')


@current_action_save
def is_shit(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id, 'Напишите что вам не понравилось')


@current_action_save
def is_shit_save_message(chat_id=None, select_item_id=None, message=None):
    current_profile = helpers.get_current_profile(chat_id)
    last_select_values = helpers.get_last_values(current_profile.last_select_values)

    if models.Rating.objects.filter(club_id=last_select_values.get('select_item_id'), chat_id=chat_id).count() == 0:
        models.Rating.objects.create(rating_value=models.RATING_SHIT, text=message.text, chat_id=chat_id,
                                     club_id=last_select_values.get('select_item_id'), created=datetime.datetime.now())
        settings.BOT.send_message(chat_id, 'Спасибо за ваш отзыв')
    else:
        settings.BOT.send_message(chat_id, 'Вы уже оставляли отзыв')


@current_action_save
def is_good(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id, 'Напишите что вам понравилось')


@current_action_save
def is_good_save_message(chat_id=None, select_item_id=None, message=None):
    current_profile = helpers.get_current_profile(chat_id)
    last_select_values = helpers.get_last_values(current_profile.last_select_values)

    if models.Rating.objects.filter(club_id=last_select_values.get('select_item_id'), chat_id=chat_id).count() == 0:
        models.Rating.objects.create(rating_value=models.RATING_GOOD, text=message.text, chat_id=chat_id,
                                     club_id=last_select_values.get('select_item_id'), created=datetime.datetime.now())
        settings.BOT.send_message(chat_id, 'Спасибо за ваш отзыв')
    else:
        settings.BOT.send_message(chat_id, 'Вы уже оставляли отзыв')


@current_action_save
def select_tag(chat_id=None, select_item_id=None, message=None):
    tag = models.Tag.objects.get(id=select_item_id)

    messages = []

    for comp_club in tag.tags_comp_clubs.all():
        buttons = helpers.get_comp_club_questionnaire_buttons(comp_club, 0,)

        keyboard = helpers.keyboard_creator_from_data_list(buttons, 3)

        file = open(comp_club.club_fotos.all().first().img.path, 'rb')
        messages.append(
            [helpers.message_creator(comp_club, ['title', 'description',], 'comp_club_questionnaire'), file,
             keyboard])

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages)
    send_messages_thread.start()


@current_action_save
def c_promotiotions(chat_id=None, select_item_id=None, message=None):



    messages = []

    request_value = select_item_id.split(':')

    print('c_promotiotions1')
    print(select_item_id.split(':'))
    print('c_promotiotions1')

    start = 0

    if len(request_value) > 1:
        print('LEN', int(request_value[1]))
        start = int(request_value[1])
        comp_club_id = request_value[0]
    else:
        comp_club_id = select_item_id
    end = start + 20

    if models.PromotionAndDiscount.objects.filter(comp_club_id=comp_club_id).count() == 0:
        settings.BOT.send_message(chat_id, models.Message.objects.filter(message_type=models.MESSAGE_PROMO_1).first().text, parse_mode="Markdown")
        return True

    for promotion in models.PromotionAndDiscount.objects.filter(comp_club_id=comp_club_id).order_by('-likes')[start:end]:
        file = open(promotion.img.path, 'rb')

        data_list = [
            ['Показать клуб 👀', 'show_club:{}'.format(promotion.comp_club.id)],
            ['❤ ({})'.format(promotion.likes), 'like_promotion:{}'.format(promotion.id)]
        ]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)

        messages.append(
            [helpers.message_creator(promotion, ['title', ], 'promotion_scheme'), file, reply_markup])

    after_thred = None

    if models.PromotionAndDiscount.objects.filter(comp_club_id=comp_club_id).count() > end:
        add_messages = []

        data_list = ['Показать еще 🌀', 'actions:{}:{}'.format(promotion.comp_club.id, end)]
        reply_markup = helpers.keyboard_creator_to_promotion(data_list)

        add_messages.append(['Еще акции', reply_markup])

        after_thred = work_treads.SendMessagesThread(chat_id, add_messages)
    else:
        add_messages = []
        reply_markup = helpers.keyboard_creator_from_data_list([], oneline=True)
        add_messages.append([models.Message.objects.filter(message_type=models.MESSAGE_PROMO_2).first().text, reply_markup])
        after_thred = work_treads.SendMessagesThread(chat_id, add_messages, parse_mode="Markdown")

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages, after_thred=after_thred)
    send_messages_thread.start()



@current_action_save
def tournaments(chat_id=None, select_item_id=None, message=None):

    messages = []

    request_value = select_item_id.split(':')

    start = 0

    if len(request_value) > 1:
        print('LEN', int(request_value[1]))
        start = int(request_value[1])
        comp_club_id = request_value[0]
    else:
        comp_club_id = select_item_id
    end = start + 20

    if models.Tournament.objects.filter(comp_club_id=comp_club_id).count() == 0:
        data_list = [
            ['Добавить частный турнир 💰', 'user_create_tournament:{}'.format(comp_club_id)]
        ]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list)
        settings.BOT.send_message(chat_id, models.Message.objects.filter(message_type=models.MESSAGE_1).first().text, reply_markup=reply_markup, parse_mode="Markdown")
        return True


    for tournament in models.Tournament.objects.filter(comp_club_id=comp_club_id, user__isnull=True).order_by('-likes')[
                      start:end]:
        file = open(tournament.img.path, 'rb')

        data_list = [
            ['Показать клуб', 'show_club:{}'.format(tournament.comp_club.id)],
            # ['❤ ({})'.format(tournament.likes), 'like_tournament:{}'.format(tournament.id)],
            ['Участвовать 💯', 'participate_tournament:{}'.format(tournament.id)]
        ]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)

        messages.append(
            [helpers.message_creator(tournament, ['title', ], 'promotion_scheme'), file, reply_markup])

    after_thred = None

    if models.Tournament.objects.filter(comp_club_id=comp_club_id).count() > end:
        add_messages = []

        data_list = ['Показать еще', 'actions:{}:{}'.format(tournament.comp_club.id, end)]
        reply_markup = helpers.keyboard_creator_to_promotion(data_list)

        add_messages.append(['Еще турниры', reply_markup])

        after_thred = work_treads.SendMessagesThread(chat_id, add_messages)
    else:
        add_messages = []
        data_list = [
            ['Добавить частный турнир 💰', 'user_create_tournament:{}'.format(comp_club_id)]
        ]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list)
        add_messages.append([models.Message.objects.filter(message_type=models.MESSAGE_2).first().text, reply_markup])
        after_thred = work_treads.SendMessagesThread(chat_id, add_messages, parse_mode="Markdown")

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages, after_thred=after_thred)
    send_messages_thread.start()

    #data_list = [
    #    ['Турниры клуба🕹', 'show_club_tournaments:{}'.format(select_item_id)],
    #    ['Частные турниры💰', 'show_user_tournaments:{}'.format(select_item_id)],
    #    ['Добавить Ч-турнир?', 'user_create_tournament:{}'.format(select_item_id)]
    #]

    #reply_markup = helpers.keyboard_creator_from_data_list(data_list)
    #settings.BOT.send_message(chat_id, 'Выбери турниры:', reply_markup=reply_markup)



@current_action_save
def reviews(chat_id=None, select_item_id=None, message=None):

    if models.Rating.objects.filter(club_id=select_item_id).count() == 0:
        settings.BOT.send_message(chat_id, 'Пока нет отзывов')

    messages = []

    for rating in models.Rating.objects.filter(club_id=select_item_id):
        messages.append(
                    [helpers.message_creator(rating, ['text', ], 'rating_scheme'), None])

    send_messages_thread = work_treads.SendMessagesThread(chat_id, messages)
    send_messages_thread.start()

    data_list = [['👍', 'is_good:{}'.format(select_item_id)], ['💩', 'is_shit:{}'.format(select_item_id)],]
    reply_markup = helpers.keyboard_creator_from_data_list(data_list)
    settings.BOT.send_message(chat_id, 'Оставить отзыв:', reply_markup=reply_markup)



@current_action_save
def add_favorite(chat_id=None, select_item_id=None, message=None):
    if models.Favorite.objects.filter(club_id=select_item_id).count() == 0:
        settings.BOT.send_message(chat_id, models.Message.objects.filter(message_type=models.MESSAGE_PROMO_3).first().text, parse_mode="Markdown")
        models.Favorite.objects.create(chat_id=chat_id, club_id=select_item_id)
    else:
        settings.BOT.send_message(chat_id, models.Message.objects.filter(message_type=models.MESSAGE_PROMO_3).first().text, parse_mode="Markdown")


@current_action_save
def pa_client_start(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    btn_notification_promotion = ['Вык акции' if telegram_profile.notification_promotion else 'Вкл акции', 'notification_promotion']
    btn_notification_tournament = ['Вык турниры' if telegram_profile.notification_tournament else 'Вкл турниры', 'notification_tournament']
    btn_notification_duel = ['Вык дуэли' if telegram_profile.notification_duel else 'Вкл дуэли', 'notification_duel']

    data_list = [
        ['Мои данные 😎', 'pac_about'], ['Мои дуэли ⚔️', 'pac_myduels'], ['Мои подписки ⭐️️', 'pac_favorites'],
        ['Мои турниры 🕹', 'my_tournaments'], ['Уведомления 🔔', 'notification_panel'], ['Друзья 👥', 'friends']
    ]
    reply_markup = helpers.keyboard_creator_from_data_list(data_list, 3)
    settings.BOT.send_message(chat_id, models.Message.objects.filter(message_type=models.MESSAGE_PERSONAL_AREA).first().text,
                              reply_markup=reply_markup, parse_mode='Markdown')


@current_action_save
def notification_panel(chat_id=None, select_item_id=None, message=None, custom_message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    btn_notification_promotion = ['Акции 🎉 ✅' if telegram_profile.notification_promotion else 'Акции 🎉 ❌', 'notification_promotion']
    btn_notification_tournament = ['Турниры 🕹 ✅' if telegram_profile.notification_tournament else 'Турниры 🕹 ❌', 'notification_tournament']
    btn_notification_duel = ['Дуэли ⚔ ️✅' if telegram_profile.notification_duel else 'Дуэли ⚔ ❌', 'notification_duel']

    data_list = [btn_notification_promotion, btn_notification_tournament, btn_notification_duel]
    reply_markup = helpers.keyboard_creator_from_data_list(data_list)


    if custom_message:
        settings.BOT.delete_message(chat_id=chat_id, message_id=message.message.message_id)
        settings.BOT.send_message(chat_id, custom_message, reply_markup=reply_markup)
        #settings.BOT.edit_message_reply_markup(chat_id=chat_id, message_id=message.message.message_id,
        #                                       reply_markup=reply_markup)
    else:
        settings.BOT.send_message(chat_id, models.Message.objects.filter(message_type=models.MESSAGE_PERSONAL_AREA_NOTIFICATIONS_BTN).first().text,
                              reply_markup=reply_markup, parse_mode='Markdown')

    if models.Favorite.objects.filter(chat_id=chat_id).count() == 0:
        settings.BOT.send_message(chat_id, 'У вас пока нет избранных клубов для получения уведомлений. Добавьте клуб в избранное ⭐️ для получения уведомлений')


@current_action_save
def pac_dada(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id, 'Ваш телеграм ID: {}'.format(chat_id))


@current_action_save
def pac_about(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

    message = helpers.make_user_info(telegram_profile)

    data_list = [
        ['Внести изменения? 🛠', 'izmemenia'],
    ]
    reply_markup = helpers.keyboard_creator_from_data_list(data_list)

    settings.BOT.send_message(chat_id, message + models.Message.objects.filter( message_type=models.MESSAGE_PERSONAL_AREA_ABOUT).first().text, parse_mode='HTML', reply_markup=reply_markup)
@current_action_save
def izmemenia(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)


    message = helpers.make_user_info(telegram_profile)
    data_list = [
        ['Изменить Ник 💬', 'nick_name_saves'],
        ['Изменить Город 🏛️', 'select_city_saves'],
        ['Изменить Steam/ Faceit 🌐', 'steam_saves'],
        ['Изменить Игры 👾', 'games_saves'],
        ['Изменить Discord/TeamSpeak  🎤', 'discord_saves'],
        ['Изменить Telegram', 'user_telegram_saves'],
    ]
    reply_markup = helpers.keyboard_creator_from_data_list(data_list, 3)
    settings.BOT.send_message(chat_id, message + models.Message.objects.filter(message_type=models.MESSAGE_PERSONAL_AREA_ABOUT).first().text, parse_mode='HTML', reply_markup=reply_markup)
@current_action_save
def nick_name_saves(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

    message = helpers.make_user_info(telegram_profile)
    settings.BOT.send_message(chat_id, message + models.Message.objects.filter(
        message_type=models.MESSAGE_PERSONAL_AREA_ABOUT).first().text, parse_mode='HTML')
    settings.BOT.send_message(chat_id, 'Введите новый никнейм')

@current_action_save
def nick_name_save(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    telegram_profile.nick_name = message.text
    telegram_profile.save()
    settings.BOT.send_message(chat_id, 'Ник сохранен')

@current_action_save
def select_city_saves(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

    message = helpers.make_user_info(telegram_profile)
    settings.BOT.send_message(chat_id, message + models.Message.objects.filter(
        message_type=models.MESSAGE_PERSONAL_AREA_ABOUT).first().text, parse_mode='HTML')
    settings.BOT.send_message(chat_id, 'Введите новый город')

@current_action_save
def select_city_save(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    telegram_profile.selected_sity = message.text
    telegram_profile.save()
    settings.BOT.send_message(chat_id, 'Город сохранен')

@current_action_save
def discord_saves(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

    message = helpers.make_user_info(telegram_profile)
    settings.BOT.send_message(chat_id, message + models.Message.objects.filter(
        message_type=models.MESSAGE_PERSONAL_AREA_ABOUT).first().text, parse_mode='HTML')
    settings.BOT.send_message(chat_id, 'Введите новый дискорд')

@current_action_save
def discord_save(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    telegram_profile.discord = message.text if message.text != 'нет' else None
    telegram_profile.save()
    settings.BOT.send_message(chat_id, 'Дискорд сохранен')

@current_action_save
def steam_saves(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

    message = helpers.make_user_info(telegram_profile)
    settings.BOT.send_message(chat_id, message + models.Message.objects.filter(
        message_type=models.MESSAGE_PERSONAL_AREA_ABOUT).first().text, parse_mode='HTML')
    settings.BOT.send_message(chat_id, 'Введите новый steam/faceit')

@current_action_save
def steam_save(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    telegram_profile.steam = message.text if message.text != 'нет' else None
    telegram_profile.save()
    settings.BOT.send_message(chat_id, 'Стим сохранен')

@current_action_save
def games_saves(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

    message = helpers.make_user_info(telegram_profile)
    settings.BOT.send_message(chat_id, message + models.Message.objects.filter(
        message_type=models.MESSAGE_PERSONAL_AREA_ABOUT).first().text, parse_mode='HTML')
    settings.BOT.send_message(chat_id, 'Введите свои игры')

@current_action_save
def games_save(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    telegram_profile.games = message.text if message.text != 'нет' else None
    telegram_profile.save()
    settings.BOT.send_message(chat_id, 'Игры сохранены')

@current_action_save
def user_telegram_saves(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

    message = helpers.make_user_info(telegram_profile)
    settings.BOT.send_message(chat_id, message + models.Message.objects.filter(
        message_type=models.MESSAGE_PERSONAL_AREA_ABOUT).first().text, parse_mode='HTML')
    settings.BOT.send_message(chat_id, 'Введите свой Telegram (Например  @Ivan_petrov')

@current_action_save
def user_telegram_save(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    telegram_profile.telegram_nick = message.text if message.text != 'нет' else None
    telegram_profile.save()
    settings.BOT.send_message(chat_id, 'Телега сохранена')

@current_action_save
def pac_about_save(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    telegram_profile.about = message.text
    telegram_profile.save()
    settings.BOT.send_message(chat_id, 'Данные сохранены')

@current_action_save
def pac_favorites(chat_id=None, select_item_id=None, message=None):
    messages = []

    if models.Favorite.objects.filter(chat_id=chat_id).count() == 0:
        settings.BOT.send_message(chat_id, 'У вас пока отсутствуют избранные клубы☹️')
    else:
        for favorite in models.Favorite.objects.filter(chat_id=chat_id):
            comp_club = favorite.club

            buttons = helpers.get_comp_club_questionnaire_buttons(comp_club, 0, float_list=True)

            keyboard = helpers.keyboard_creator_from_data_list_float(buttons)

            club_url = 'https://telegra.ph/'

            if comp_club.club_url:
                club_url = comp_club.club_url

            btn_list = [telebot.types.InlineKeyboardButton(text='Фото 📷/ Тарифы 💰/ Железо 🖥',
                                                           url=club_url)]
            keyboard.add(*btn_list)
            btn_list = [
                telebot.types.InlineKeyboardButton(text='Забронировать 🗿', url=club_url),
            ]
            keyboard.add(*btn_list)

            btn_list = [telebot.types.InlineKeyboardButton(text='Удалить из избранного ❌',
                                                           callback_data='del_clubfavorite:{}'.format(favorite.club.id))]
            keyboard.add(*btn_list)

            file = open(comp_club.club_fotos.all().first().img.path, 'rb')
            messages.append(
                [helpers.message_creator(comp_club, ['title', 'description',], 'comp_club_questionnaire'), file,
                 keyboard])

        add_messages = []

        reply_markup = telebot.types.InlineKeyboardMarkup()

        add_messages.append([models.Message.objects.filter(message_type=models.MESSAGE_PERSONAL_AREA_FAVORITES_BTN).first().text, reply_markup])

        after_thred = work_treads.SendMessagesThread(chat_id, add_messages)

        send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages, after_thred=after_thred)
        send_messages_thread.start()


@current_action_save
def pac_mytournaments(chat_id=None, select_item_id=None, message=None):
    messages = []

    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

    if models.Duel.objects.filter(user=telegram_profile).count() == 0:
        settings.BOT.send_message(chat_id, 'Нет дуэлей')
    else:
        for duel in models.Duel.objects.filter(user=telegram_profile):
            keyboard = helpers.call_back_buttons_creator([['Удалить', 'ct_delete', duel.id]])

            messages.append(
                [helpers.message_creator(duel, ['title', ], 'duel_scheme'), keyboard])

            send_messages_thread = work_treads.SendMessagesThread(chat_id, messages)
            send_messages_thread.start()


@current_action_save
def pa_club_start(chat_id=None, select_item_id=None, message=None):
    data_list = [
        ['Создать/Удалить турнир🕹', 'ccc_tour'], ['Создать/Удалить Акцию🎉', 'ccc_promotion'],
        ['Изменить описание', 'edit_desc_club'], ['Мой клуб', 'my_club'],
    ]
    reply_markup = helpers.keyboard_creator_from_data_list(data_list)
    settings.BOT.send_message(chat_id, 'Личный кабинет клуба:', reply_markup=reply_markup)


@current_action_save
def ccc_tour(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    messages = []

    comp_club = telegram_profile.employees_clubs.all().first()

    if telegram_profile.employees_clubs.count() == 0:
        settings.BOT.send_message(chat_id, 'Клуб не прошел идентификацию')
    else:
        for tournament in models.Tournament.objects.filter(comp_club_id=comp_club.id):
            file = open(tournament.img.path, 'rb')

            data_list = [
                ['Удалить турнир', 'delete_tournament:{}'.format(tournament.id)],
            ]
            reply_markup = helpers.keyboard_creator_from_data_list(data_list)

            messages.append(
                [helpers.message_creator(tournament, ['title', ], 'promotion_scheme'), file, reply_markup])

        send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages)
        send_messages_thread.start()

        messages = []
        messages.append(
            ['Для добавления. Введите даные о турнире.', None])

        send_messages_thread = work_treads.SendMessagesThread(chat_id, messages, 1)
        send_messages_thread.start()


@current_action_save
def comp_club_card(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id, 'Введите номер клуба')


@current_action_save
def comp_club_active(chat_id=None, select_item_id=None, message=None):
    try:
        сomp_сlub = models.CompClub.objects.get(club_id=message.text)

        telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

        сomp_сlub.is_identified = True
        сomp_сlub.employees.add(telegram_profile)
        сomp_сlub.save()
        settings.BOT.send_message(chat_id, 'Клуб активирован')
    except (ObjectDoesNotExist, ValidationError):
        settings.BOT.send_message(chat_id, 'Вы ввели не верный идентификатор')

    print(message.text)


@current_action_save
def comp_club_save_tour(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

    comp_club = telegram_profile.employees_clubs.all().first()

    fileID = message.photo[-1].file_id
    file_info = settings.BOT.get_file(fileID)
    downloaded_file = settings.BOT.download_file(file_info.file_path)

    file_full_path = 'https://api.telegram.org/file/bot{}/{}'.format(settings.BOT.token, file_info.file_path)

    print(file_full_path)

    r = requests.get(file_full_path)

    with open("image.jpg", 'wb') as f:
        f.write(r.content)

    reopen = open('image.jpg', 'rb')
    django_file = File(reopen)

    # file = File(open('https://api.telegram.org/file/bot497044489:AAHS3JFCl7audWdw55sdpOvhr2l5wYWlv04/photos/file_0.jpg', 'rb'))

    print(type(downloaded_file))
    tournament = models.Tournament.objects.create(comp_club=comp_club, title=message.caption, img=django_file)

    settings.BOT.send_message(chat_id, 'Турнир создан')

    messages = []

    file = open(tournament.img.path, 'rb')

    messages.append(
        [helpers.message_creator(tournament, ['title', ], 'promotion_scheme'), file, None])

    for favorite in comp_club.club_favorites.all():
        client = models.TelegramProfile.objects.get(chat_id=favorite.chat_id)
        if client.notification_tournament:
            send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages)
            send_messages_thread.start()


@current_action_save
def ccc_promotion(chat_id=None, select_item_id=None, message=None):
    messages = []
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    comp_club = telegram_profile.employees_clubs.all().first()

    for promotion in models.PromotionAndDiscount.objects.filter(comp_club_id=comp_club.id):
        file = open(promotion.img.path, 'rb')

        data_list = [
            ['Удалить акцию', 'delete_promotion:{}'.format(promotion.id)],
        ]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list)

        messages.append(
            [helpers.message_creator(promotion, ['title', ], 'promotion_scheme'), file, reply_markup])

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages)
    send_messages_thread.start()

    messages = []
    messages.append(
        ['Для добавления. Введите даные об акции.', None])

    send_messages_thread = work_treads.SendMessagesThread(chat_id, messages, 1)
    send_messages_thread.start()


@current_action_save
def ccc_promotion_save(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

    comp_club = telegram_profile.employees_clubs.all().first()

    fileID = message.photo[-1].file_id
    file_info = settings.BOT.get_file(fileID)
    downloaded_file = settings.BOT.download_file(file_info.file_path)

    file_full_path = 'https://api.telegram.org/file/bot{}/{}'.format(settings.BOT.token, file_info.file_path)

    print(file_full_path)

    r = requests.get(file_full_path)

    with open("image.jpg", 'wb') as f:
        f.write(r.content)

    reopen = open('image.jpg', 'rb')
    django_file = File(reopen)

    #file = File(open('https://api.telegram.org/file/bot497044489:AAHS3JFCl7audWdw55sdpOvhr2l5wYWlv04/photos/file_0.jpg', 'rb'))



    promotion = models.PromotionAndDiscount.objects.create(comp_club=comp_club, title=message.caption, img=django_file)

    print('FAVORITESSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSs')
    print(comp_club.club_favorites.all())
    print('FAVORITESSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSs')

    messages = []


    file = open(promotion.img.path, 'rb')

    messages.append(
            [helpers.message_creator(promotion, ['title', ], 'promotion_scheme'), file, None])

    settings.BOT.send_message(chat_id, 'Акция создана')

    for favorite in comp_club.club_favorites.all():
        client = models.TelegramProfile.objects.get(chat_id=favorite.chat_id)
        if client.notification_promotion:
            send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages)
            send_messages_thread.start()




@current_action_save
def clientc_tour(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id, 'Введите данные о туре')

@current_action_save
def get_phone(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id, 'Номер телефона: {}'.format(select_item_id))


@current_action_save
def pa_selector(chat_id=None, select_item_id=None, message=None):
    data_list = [['Вы клиент? 🧑', 'pa_client_start'], ['Вы клуб? 💻', 'pa_club_start']]
    reply_markup = helpers.keyboard_creator_from_data_list(data_list)
    settings.BOT.send_message(chat_id, 'Выбор кабинета', reply_markup=reply_markup)


@current_action_save
def cclient_tour(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id, 'Введите данные о туре')


@current_action_save
def client_save_tour(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    models.Tournament.objects.create(user=telegram_profile, title=message.text)

    settings.BOT.send_message(chat_id, 'Дуэль создана')


@current_action_save
def ct_delete(chat_id=None, select_item_id=None, message=None):
    duel = models.Duel.objects.get(id=select_item_id)
    duel.delete()
    settings.BOT.send_message(chat_id, 'Дуэль удалена')


@current_action_save
def notification_promotion(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    telegram_profile.notification_promotion = False if telegram_profile.notification_promotion else True

    if telegram_profile.notification_promotion:
        custom_message = 'Вы только что включили уведомления о новых акциях 🎉️'
        #settings.BOT.send_message(chat_id, 'Вы только что включили уведомления о новых акциях 🎉️')
    else:
        custom_message = 'Вы только что отключили уведомления о новых акциях 🎉'
        #settings.BOT.send_message(chat_id, 'Вы только что отключили уведомления о новых акциях 🎉')

    telegram_profile.save()
    notification_panel(chat_id=chat_id, select_item_id=select_item_id, message=message, custom_message=custom_message)


@current_action_save
def notification_tournament(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    telegram_profile.notification_tournament = False if telegram_profile.notification_tournament else True

    if telegram_profile.notification_tournament:
        custom_message = 'Вы только что включили уведомления о новых турнирах 🕹'
        #settings.BOT.send_message(chat_id, 'Вы только что включили уведомления о новых турнирах 🕹')
    else:
        custom_message = 'Вы только что отключили уведомления о новых турнирах 🕹'
        #settings.BOT.send_message(chat_id, 'Вы только что отключили уведомления о новых турнирах 🕹')

    telegram_profile.save()
    notification_panel(chat_id=chat_id, select_item_id=select_item_id, message=message, custom_message=custom_message)


@current_action_save
def notification_duel(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    telegram_profile.notification_duel = False if telegram_profile.notification_duel else True

    if telegram_profile.notification_duel:
        custom_message = 'Вы только что включили уведомления о новых дуэлях ⚔️'
        #settings.BOT.send_message(chat_id, 'Вы только что включили уведомления о новых дуэлях ⚔️')
    else:
        custom_message = 'Вы только что отключили уведомления о новых дуэлях ⚔️'
        #settings.BOT.send_message(chat_id, 'Вы только что отключили уведомления о новых дуэлях ⚔️')

    telegram_profile.save()
    notification_panel(chat_id=chat_id, select_item_id=select_item_id, message=message, custom_message=custom_message)


@current_action_save
def all_promotions(chat_id=None, select_item_id=None, message=None):

    messages = []

    start = 0

    if select_item_id:
        start = int(select_item_id)

    end = start + 20

    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

    if telegram_profile.selected_sity:
        selected_sity = models.City.objects.get(title=telegram_profile.selected_sity)
    else:
        selected_sity = models.City.objects.get(title='москва')
    selected_sity

    club_ids = [city.id for city in models.CompClub.objects.filter(city_id=selected_sity.id)]

    if models.PromotionAndDiscount.objects.filter(comp_club_id__in=club_ids).count() == 0:
        settings.BOT.send_message(chat_id, 'Пока акции и скидки в вашем городе отсутствуют 😭')
    else:
        settings.BOT.send_message(chat_id, 'Акции и скидки cyber-клубов вашего города 🎉')

    for promotion in models.PromotionAndDiscount.objects.filter(comp_club_id__in=club_ids).order_by('-likes')[start:end]:
        file = open(promotion.img.path, 'rb')

        data_list = [
            ['Показать клуб  👀', 'show_club:{}'.format(promotion.comp_club.id)],
            ['❤ ({})'.format(promotion.likes), 'like_promotion:{}'.format(promotion.id)]
        ]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)

        messages.append(
            ['Акция от клуба\n{}\n {}'.format(promotion.comp_club.title, helpers.message_creator(promotion, ['title', ], 'promotion_scheme')), file, reply_markup])

    after_thred = None

    if models.PromotionAndDiscount.objects.all().count() > end:
        add_messages = []

        data_list = ['Показать еще 🌀', 'actions:{}:{}'.format(promotion.comp_club.id, end)]
        reply_markup = helpers.keyboard_creator_to_promotion(data_list)

        add_messages.append(['Еще акции', reply_markup])

        after_thred = work_treads.SendMessagesThread(chat_id, add_messages)
    else:
        add_messages = []
        reply_markup = telebot.types.InlineKeyboardMarkup()
        add_messages.append([models.Message.objects.filter(message_type=models.MESSAGE_PROMO_4).first().text, reply_markup])

        after_thred = work_treads.SendMessagesThread(chat_id, add_messages)

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages, after_thred=after_thred)
    send_messages_thread.start()



@current_action_save
def all_clubs(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id,
                              'Cyber-клубы твоего города! В списке представлены не все клубы? Тогда жми сюда 💰 - https://telegra.ph/Free-Drop-04-28. Не забывайте про хештеги!')

    time.sleep(1)
    messages = []

    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

    if telegram_profile.selected_sity:
        try:
            selected_sity = models.City.objects.get(title=telegram_profile.selected_sity)
            comp_clubs = models.CompClub.objects.filter(city_id=selected_sity.id)[0:20]
            clubs_count = models.CompClub.objects.filter(city_id=selected_sity.id).count()
        except ObjectDoesNotExist:
            comp_clubs = models.CompClub.objects.all()[0:20]
            clubs_count = models.CompClub.objects.count()

    for comp_club in comp_clubs:
        buttons = helpers.get_comp_club_questionnaire_buttons(comp_club, 0, float_list=True)

        keyboard = helpers.keyboard_creator_from_data_list_float(buttons)

        club_url = 'https://telegra.ph/'

        if comp_club.club_url:
            club_url = comp_club.club_url

        btn_list = [
            telebot.types.InlineKeyboardButton(text='Фото 📷/ Тарифы 💰/ Железо 🖥',
                                                       url=club_url),
        ]
        keyboard.add(*btn_list)

        btn_list = [
            telebot.types.InlineKeyboardButton(text='Забронировать 🗿', url=club_url),
        ]
        keyboard.add(*btn_list)

        try:
            file = open(comp_club.club_fotos.all().first().img.path, 'rb')
        except AttributeError:
            file = open('/home/comp_club/photo-not-available.jpg', 'rb')
        messages.append(
            [helpers.message_creator(comp_club, ['title', 'description',], 'comp_club_questionnaire'), file,
             keyboard])

    after_thred = None



    if clubs_count > 20:
        add_messages = []

        data_list = [['Показать еще 🌀', 'all_clubs_next:20']]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list)

        add_messages.append(['Еще комп-клубы {}'.format((clubs_count - 20)), reply_markup])

        after_thred = work_treads.SendMessagesThread(chat_id, add_messages)

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages, after_thred=after_thred)
    send_messages_thread.start()







@current_action_save
def iamclub(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    if telegram_profile.employees_clubs.count() == 0:
        comp_club_card(chat_id=chat_id, select_item_id=select_item_id, message=message)
    else:
        pa_club_start(chat_id=chat_id, select_item_id=select_item_id, message=message)


@current_action_save
def call_club(chat_id=None, select_item_id=None, message=None):
    comp_club = models.CompClub.objects.get(id=select_item_id)
    settings.BOT.send_message(chat_id, 'Номер телефона: {}'.format(comp_club.phone))


@current_action_save
def delete_promotion(chat_id=None, select_item_id=None, message=None):
    promotion = models.PromotionAndDiscount.objects.get(id=select_item_id)
    promotion.delete()
    settings.BOT.send_message(chat_id, 'Акция удалена')


@current_action_save
def delete_tournament(chat_id=None, select_item_id=None, message=None):
    tournament = models.Tournament.objects.get(id=select_item_id)
    tournament.delete()
    settings.BOT.send_message(chat_id, 'Турнир удалена')


@current_action_save
def my_club(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    comp_club = telegram_profile.employees_clubs.all().first()
    settings.BOT.send_message(chat_id, comp_club.description)


@current_action_save
def edit_desc_club(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id, 'Введите новое описание')


@current_action_save
def edit_desc_club_save(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    comp_club = telegram_profile.employees_clubs.all().first()
    comp_club.description = message.text
    comp_club.save()
    settings.BOT.send_message(chat_id, 'Описание изменено')


@current_action_save
def all_promotion(chat_id=None, select_item_id=None, message=None):
    messages = []

    for promotion in models.PromotionAndDiscount.objects.all():
        file = open(promotion.img.path, 'rb')

        messages.append(
            ['Акция от клуба \n{}'.format(helpers.message_creator(promotion, ['title', ], 'promotion_scheme')), file, None])

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages)
    send_messages_thread.start()


@current_action_save
def all_tournament(chat_id=None, select_item_id=None, message=None):
    data_list = [
        ['Турниры клуба🕹', 'tours_clubs'],
        ['Частные турниры💰', 'privat_tours'],
        ['По играм 🎮', 'tours_listgames']
    ]
    data_list = [
        ['Турниры клуба🕹', 'tours_clubs'],
        ['Частные турниры💰', 'privat_tours'],
    ]
    reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=False)

    settings.BOT.send_message(chat_id, models.Message.objects.filter(message_type=models.MESSAGE_TOURNAMENT_1).first().text, reply_markup=reply_markup, parse_mode='Markdown')
    return True

    messages = []

    start = 0

    if select_item_id:
        start = int(select_item_id)

    end = start + 10

    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

    if telegram_profile.selected_sity:
        selected_sity = models.City.objects.get(title=telegram_profile.selected_sity)
    else:
        selected_sity = models.City.objects.get(title='москва')
    selected_sity

    club_ids = [city.id for city in models.CompClub.objects.filter(city_id=selected_sity.id)]

    for tournament in models.Tournament.objects.filter(comp_club_id__in=club_ids).order_by('-likes')[start:end]:
        file = open(tournament.img.path, 'rb')

        data_list = [
            ['Показать клуб  👀', 'show_club:{}'.format(tournament.comp_club.id)],
            ['❤ ({})'.format(tournament.likes), 'like_tournament:{}'.format(tournament.id)],
            ['Участвовать 💯', 'participate_tournament:{}'.format(tournament.id)]
        ]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)

        messages.append(
            [helpers.message_creator(tournament, ['title', ], 'promotion_scheme'), file, reply_markup])

    after_thred = None

    if models.Tournament.objects.all().count() > end:
        add_messages = []

        data_list = ['Показать еще 🌀', 'all_tournament:{}'.format(end)]
        reply_markup = helpers.keyboard_creator_to_promotion(data_list)

        add_messages.append(['Еще турниры', reply_markup])

        after_thred = work_treads.SendMessagesThread(chat_id, add_messages)

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages, after_thred=after_thred)
    send_messages_thread.start()


@current_action_save
def all_clubs_next(chat_id=None, select_item_id=None, message=None):
    messages = []

    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

    start = int(select_item_id)
    end = start + 20




    try:
        selected_sity = models.City.objects.get(title=telegram_profile.selected_sity)
    except ObjectDoesNotExist:
        selected_sity = models.City.objects.get(title='москва')


    for comp_club in models.CompClub.objects.filter(city_id=selected_sity.id)[start:end]:
        buttons = helpers.get_comp_club_questionnaire_buttons(comp_club, 0, float_list=True)

        #if models.CompClub.objects.count() > end and len(messages) == 19:
        #    buttons.append([['Показать еще', 'all_clubs_next:{}'.format(end)]])

        keyboard = helpers.keyboard_creator_from_data_list_float(buttons)

        club_url = 'https://telegra.ph/'

        if comp_club.club_url:
            club_url = comp_club.club_url

        btn_list = [telebot.types.InlineKeyboardButton(text='Фото 📷/ Тарифы 💰/ Железо 🖥',
                                                       url=club_url)]
        keyboard.add(*btn_list)
        btn_list = [
            telebot.types.InlineKeyboardButton(text='Забронировать 🗿', url=club_url),
        ]
        keyboard.add(*btn_list)

        try:
            file = open(comp_club.club_fotos.all().first().img.path, 'rb')
        except AttributeError:
            file = open('/home/comp_club/photo-not-available.jpg', 'rb')

        messages.append(
            [helpers.message_creator(comp_club, ['title', 'description', ], 'comp_club_questionnaire'), file,
             keyboard])


    after_thred = None

    club_count = models.CompClub.objects.filter(city_id=selected_sity.id).count()

    if club_count > end:
        add_messages = []

        data_list = [['Показать еще 🌀', 'all_clubs_next:{}'.format(end)]]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list)

        add_messages.append(['Еще комп-клубы {}'.format((club_count - end)), reply_markup])

        after_thred = work_treads.SendMessagesThread(chat_id, add_messages)

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages, after_thred=after_thred)
    send_messages_thread.start()


@current_action_save
def duels(chat_id=None, select_item_id=None, message=None):
    if models.Duel.objects.filter(comp_club_id=select_item_id).count() == 0:
        settings.BOT.send_message(chat_id, models.Message.objects.filter(message_type=models.MESSAGE_DUEL_2).first().text, parse_mode='Markdown')
    else:
        messages = []



        now = timezone.now()


        for duel in models.Duel.objects.filter(comp_club_id=select_item_id):
            if duel.create_date < (now - datetime.timedelta(days=1)):
                duel.delete()
                continue

            duel_participants = models.MyDuel.objects.filter(duel_id=duel.id).count()

            if duel.number_participants and duel_participants >= duel.number_participants:
                continue

            data_list = [['{}'.format('Присоеденится {}/{}'.format(duel_participants, duel.number_participants) if duel.number_participants else 'Принять вызов ⚔️'), 'take_challenge:{}'.format(duel.id)]]
            reply_markup = helpers.keyboard_creator_from_data_list(data_list)

            messages.append(
                    [helpers.message_creator(duel, ('id', 'title', 'get_hours', 'get_create_date', 'get_type'), 'duel_scheme'), reply_markup])


        send_messages_thread = work_treads.SendMessagesThread(chat_id, messages)
        send_messages_thread.start()

    messages = []

    data_list = [['Добавить дуэль ➕ ⚔', 'add_duel:{}'.format(select_item_id)]]
    reply_markup = helpers.keyboard_creator_from_data_list(data_list)

    if models.Duel.objects.filter(comp_club_id=select_item_id).count() == 0:
        messages.append(
            [models.Message.objects.filter(message_type=models.MESSAGE_DUEL_3).first().text, reply_markup])
    else:
        messages.append(
            [models.Message.objects.filter(message_type=models.MESSAGE_DUEL_1).first().text, reply_markup])

    send_messages_thread = work_treads.SendMessagesThread(chat_id, messages, 1)
    send_messages_thread.start()


@current_action_save
def save_duel(chat_id=None, select_item_id=None, message=None):
    current_profile = helpers.get_current_profile(chat_id)
    last_select_values = helpers.get_last_values(current_profile.last_select_values)

    duel = models.Duel.objects.create(title=message.text, comp_club_id=last_select_values.get('select_item_id'), user=current_profile)

    for favorite in models.Favorite.objects.filter(club=duel.comp_club):
        if str(chat_id) != str(favorite.chat_id):
            models.NewDuelMessage.objects.create(duel=duel, chat_id=favorite.chat_id)



    settings.BOT.send_message(chat_id, 'Дуэль создана')

@current_action_save
def tours_games(chat_id=None, select_item_id=None, message=None):
    messages = []

    start = 0

    if select_item_id:
        start = int(select_item_id)

    end = start + 10

    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

    if telegram_profile.selected_sity:
        selected_sity = models.City.objects.get(title=telegram_profile.selected_sity)
    else:
        selected_sity = models.City.objects.get(title='москва')
    selected_sity

    club_ids = [city.id for city in models.CompClub.objects.filter(city_id=selected_sity.id)]

    if models.Tournament.objects.filter(comp_club_id__in=club_ids, title__icontains=message.text).count() == 0:
        settings.BOT.send_message(chat_id, 'Турниров по данной игре нет☹️')
        return True

    for tournament in models.Tournament.objects.filter(comp_club_id__in=club_ids).order_by('-likes')[start:end]:
        file = open(tournament.img.path, 'rb')

        data_list = [
            ['Показать клуб  👀', 'show_club:{}'.format(tournament.comp_club.id)],
            ['❤ ({})'.format(tournament.likes), 'like_tournament:{}'.format(tournament.id)],
            ['Участвовать 💯', 'participate_tournament:{}'.format(tournament.id)]
        ]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)

        messages.append(
            [helpers.message_creator(tournament, ['title', ], 'promotion_scheme'), file, reply_markup])

    after_thred = None

    if models.Tournament.objects.all().count() > end:
        add_messages = []

        data_list = ['Показать еще 🌀', 'all_tournament:{}'.format(end)]
        reply_markup = helpers.keyboard_creator_to_promotion(data_list)

        add_messages.append(['Еще турниры', reply_markup])

        after_thred = work_treads.SendMessagesThread(chat_id, add_messages)

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages, after_thred=after_thred)
    send_messages_thread.start()


@current_action_save
def hash_search(chat_id=None, select_item_id=None, message=None):
    messages = []

    l = message.text.split('#')

    work_l = []

    for i in l:
        if len(i) > 0:
            work_l.append('#{}'.format(i))




    if '#дуэль' in work_l:
        comp_club_list = models.CompClub.objects.filter(reduce(operator.and_, (Q(hash_tags__contains=x) for x in work_l)))
        if len(comp_club_list) == 0:
            settings.BOT.send_message(chat_id, '«Дуэлей нет или проверьте правильность написания тут - telegra.ph/HESHTEGI-03-27»')
        else:
            duel_list = models.Duel.objects.filter(comp_club_id__in=[comp_club.id for comp_club in comp_club_list])
            messages = []

            for duel in duel_list:
                messages.append(
                    [helpers.message_creator(duel, ['title', ], 'duel_scheme')])

                send_messages_thread = work_treads.SendMessagesThread(chat_id, messages)
                send_messages_thread.start()



    else:
        print('city_hasescity_hasescity_hasescity_hasescity_hasescity_hasescity_hasescity_hasescity_hasescity_hases')
        city_hases = ['#краснодар', '#махачкала', '#санктпетербург', '#москва', '#казань']

        city_hases = ['#{}'.format(city.title) for city in models.City.objects.all()]
        print('city_hasescity_hasescity_hasescity_hasescity_hasescity_hasescity_hasescity_hasescity_hasescity_hases')

        if len([el for el in work_l if el in city_hases]) == 0:
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            city_list_exclude = [city.id for city in models.City.objects.all().exclude(title=telegram_profile.selected_sity)]
        else:
            telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
            current_sended_city = [el for el in work_l if el in city_hases][0]
            telegram_profile.selected_sity = current_sended_city[1:]
            telegram_profile.save()

            data_list = [['О сервисе 🤖', 'btn_about_s:1']]
            reply_markup = helpers.keyboard_creator_from_data_list(data_list)

            messages.append(
                ['Хотите добавить дуэль?', reply_markup])

            settings.BOT.send_message(chat_id, 'Отлично! Уделите 3 минуты вашего времени, чтобы ознакомиться с нашим Ботом',
                                      reply_markup=reply_markup)

            return True
            #city_list_exclude = [city.id for city in
            #                     models.City.objects.all().exclude(title__in=[el[1:] for el in work_l if el in city_hases])]
            #print([el[1:] for el in work_l if el in city_hases], '<=========')

        print(city_list_exclude)

        comp_club_list = models.CompClub.objects.filter(reduce(operator.and_, (Q(hash_tags__contains=x) for x in work_l)))
        if len(comp_club_list) == 0:
            if models.City.objects.filter(title=work_l[0][1:]).count() > 0:
                selected_city = models.City.objects.filter(title=work_l[0][1:]).first()
                telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
                telegram_profile.selected_sity = selected_city.title
                telegram_profile.save()
                settings.BOT.send_message(chat_id, 'Мы сохранили ваш выбор')
            else:
                if models.Game.objects.filter(title=message.text).count() == 0:
                    settings.BOT.send_message(chat_id, '«клубов нет или проверьте правильность написания тут - telegra.ph/HESHTEGI-03-27»')
                else:
                    tours_games(chat_id, select_item_id, message)
        else:

            club_count = models.CompClub.objects.filter(reduce(operator.and_, (Q(hash_tags__contains=x) for x in work_l))).exclude(city_id__in=city_list_exclude).count()
            last_message = models.Message.objects.filter(message_type=models.LAST_MESSAGE_HASH_SEARCH).first().text.format(user_request=message.text, city=telegram_profile.selected_sity,
                                                                                                                           count=club_count)

            for comp_club in models.CompClub.objects.filter(reduce(operator.and_, (Q(hash_tags__contains=x) for x in work_l))).exclude(city_id__in=city_list_exclude):
                buttons = helpers.get_comp_club_questionnaire_buttons(comp_club, 0, float_list=True)

                keyboard = helpers.keyboard_creator_from_data_list_float(buttons)

                club_url = 'https://telegra.ph/'

                if comp_club.club_url:
                    club_url = comp_club.club_url

                btn_list = [telebot.types.InlineKeyboardButton(text='Фото 📷/ Тарифы 💰/ Железо 🖥',
                                                               url=club_url) ]
                keyboard.add(*btn_list)
                btn_list = [
                    telebot.types.InlineKeyboardButton(text='Забронировать 🗿', url=club_url),
                ]
                keyboard.add(*btn_list)

                try:
                    file = open(comp_club.club_fotos.all().first().img.path, 'rb')
                    messages.append(
                        [helpers.message_creator(comp_club, ['title', 'description',], 'comp_club_questionnaire'), file,
                        keyboard])
                except AttributeError:
                    pass



    add_messages = []
    data_list = []
    reply_markup = telebot.types.InlineKeyboardMarkup()

    add_messages.append([last_message, reply_markup])
    after_thred = work_treads.SendMessagesThread(chat_id, add_messages)

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages, after_thred=after_thred)
    send_messages_thread.start()


@current_action_save
def about_service(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id, models.Message.objects.filter(message_type=models.MESSAGE_ABOUT_SERVICE).first().text, parse_mode='Markdown')


@current_action_save
def service_contacts(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id, '☎️ +7(977)-467-29-00\n☎️ +7(966)-133-34-44\n🤝 @F2F_Official\n💌 friends2fight@gmail.com')

@current_action_save
def service_instruction(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id, '🔶 Как выбирать город? https://telegra.ph/Kak-vybirat-gorod-05-08\n🔶 Как пользоваться хештегами? https://telegra.ph/Kak-polzovatsya-heshtegami-05-08')

@current_action_save
def service_partners(chat_id=None, select_item_id=None, message=None):
    reply_markup = telebot.types.InlineKeyboardMarkup()

    btn_list = [telebot.types.InlineKeyboardButton(text='Новые клубы',
                                                   url='https://telegra.ph/Free-Drop-04-28'),
                telebot.types.InlineKeyboardButton(text='Сотрудничество',
                                                   url='https://t.me/F2F_Official '),
                ]
    reply_markup.add(*btn_list)
    settings.BOT.send_message(chat_id, 'Пиши @F2F_Official "Хочу к вам в команду ❤️"', reply_markup=reply_markup)


@current_action_save
def like_promotion(chat_id=None, select_item_id=None, message=None):
    current_profile = helpers.get_current_profile(chat_id)
    promotion = models.PromotionAndDiscount.objects.get(id=select_item_id)

    if models.PromotionLike.objects.filter(promotion=promotion, profile=current_profile).count() > 0:
        settings.BOT.send_message(chat_id, 'Вы уже оценивали данную акцию')
    else:
        promotion.likes = promotion.likes + 1
        promotion.save()

        models.PromotionLike.objects.create(promotion=promotion, profile=current_profile)

        settings.BOT.send_message(chat_id, 'Спасибо за оценку')


@current_action_save
def show_club(chat_id=None, select_item_id=None, message=None):
    messages = []

    for comp_club in models.CompClub.objects.filter(id=select_item_id):
        buttons = helpers.get_comp_club_questionnaire_buttons(comp_club, 0, float_list=True)

        keyboard = helpers.keyboard_creator_from_data_list_float(buttons)

        club_url = 'https://telegra.ph/'

        if comp_club.club_url:
            club_url = comp_club.club_url

        btn_list = [telebot.types.InlineKeyboardButton(text='Фото 📷/ Тарифы 💰/ Железо 🖥',
                                                       url=club_url)]
        keyboard.add(*btn_list)
        btn_list = [
            telebot.types.InlineKeyboardButton(text='Забронировать 🗿', url=club_url),
        ]
        keyboard.add(*btn_list)

        file = open(comp_club.club_fotos.all().first().img.path, 'rb')
        messages.append(
            [helpers.message_creator(comp_club, ['title', 'description', ], 'comp_club_questionnaire'), file,
             keyboard])

    after_thred = None

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages, after_thred=after_thred)
    send_messages_thread.start()

@current_action_save
def like_tournament(chat_id=None, select_item_id=None, message=None):
    current_profile = helpers.get_current_profile(chat_id)
    tournament = models.Tournament.objects.get(id=select_item_id)

    if models.TournamentLike.objects.filter(tournament=tournament, profile=current_profile).count() > 0:
        settings.BOT.send_message(chat_id, 'Вы уже оценивали данный турнир')
    else:
        tournament.likes = tournament.likes + 1
        tournament.save()

        models.TournamentLike.objects.create(tournament=tournament, profile=current_profile)

        settings.BOT.send_message(chat_id, 'Спасибо за оценку')


@current_action_save
def friends(chat_id=None, select_item_id=None, message=None):
    if models.Friend.objects.filter(own=chat_id).count() == 0:
        settings.BOT.send_message(chat_id, 'У вас пока нет друзей 😭')
    else:
        settings.BOT.send_message(chat_id, '{}'.format(
            models.Message.objects.filter(message_type=models.MESSAGE_PERSONAL_AREA_FRIENDS_BTN).first().text))
        for f in models.Friend.objects.filter(own=chat_id):
            telegram_profile = models.TelegramProfile.objects.get(chat_id=f.friend)

            #chat = settings.BOT.get_chat(f.friend)
            #user_name_to_btn = helpers.make_user_name(chat)
            reply_markup = telebot.types.InlineKeyboardMarkup()


            #btn_list = [telebot.types.InlineKeyboardButton(text=user_name_to_btn,
            #                                               callback_data='user_info_from_profile:{}'.format(
            #                                                   telegram_profile.chat_id))]

            user_info = helpers.make_user_info(telegram_profile)

            data_list = [['Сообщение 💬', 'write_message_to_duel_author:{}'.format(telegram_profile.id)],
                         ['Убрать из друзей ❌', 'friend_del:{}'.format(telegram_profile.chat_id)]]
            data_list = [['Убрать из друзей ❌', 'friend_del:{}'.format(telegram_profile.chat_id)]]
            reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)
            settings.BOT.send_message(chat_id, user_info, reply_markup=reply_markup)
            #reply_markup.add(*btn_list)



        #print([friend.friend for friend in models.Friend.objects.filter(own=chat_id)])
        #settings.BOT.send_message(chat_id, '{} \nЧто бы добавить друга, введие его id'.format(
        #    '\n'.join([friend.friend for friend in models.Friend.objects.filter(own=chat_id)])))


@current_action_save
def friend_save(chat_id=None, select_item_id=None, message=None):
    try:

        models.TelegramProfile.objects.get(chat_id=message.text)

        if models.Friend.objects.filter(own=chat_id, friend=message.text).count() == 0:
            telegram_profile = models.TelegramProfile.objects.get(chat_id=message.text)
            data_list = [['Сообщение', 'message_tofriend'],
                         ['Добавить в друзья', 'friend_confirmation:{}'.format(telegram_profile.id)]]
            reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)
            models.Friend.objects.create(own=chat_id, friend=telegram_profile.id)
            settings.BOT.send_message(chat_id, 'Данные о вашем друге', reply_markup=reply_markup)
        else:
            settings.BOT.send_message(chat_id, 'Вы уже добавляли этого друга')
    except ObjectDoesNotExist:
        settings.BOT.send_message(chat_id, 'К сожалению ваш друг не зарегестрирован у нас')


@current_action_save
def friend_confirmation(chat_id=None, select_item_id=None, message=None):
    tp = models.TelegramProfile.objects.get(id=select_item_id)

    try:
        models.Friend.objects.get(own=chat_id, friend=tp.chat_id)
    except ObjectDoesNotExist:
        models.Friend.objects.create(own=chat_id, friend=tp.chat_id)
        settings.BOT.send_message(chat_id, 'Новый друг успешно добавлен')
        settings.BOT.send_message(tp.chat_id, '{} добавил вас в друзья'.format(chat_id))


@current_action_save
def send_message_to_user(chat_id=None, select_item_id=None, message=None):
        telegram_id, *args = message.text[1:].split(' ')
        print(telegram_id, 'FFFFFFFFFF', args)
        if len(args):
            settings.BOT.send_message(telegram_id, 'Сообщение от пользователя {}\n{}'.format(chat_id, ' '.join([msg for msg in args])))
        else:
            try:
                telegram_profile = models.TelegramProfile.objects.get(chat_id=telegram_id)
                data_list = [['Сообщение 💬', 'write_message_to_duel_author:{}'.format(telegram_profile.id)],
                         ['Добавить в друзья 👥', 'friend_confirmation:{}'.format(telegram_profile.id)]]
                user_info = helpers.make_user_info(telegram_profile)
                reply_markup = helpers.keyboard_creator_from_data_list(data_list)
                settings.BOT.send_message(chat_id, user_info, reply_markup=reply_markup)
            except ObjectDoesNotExist:
                settings.BOT.send_message(chat_id, 'Игрок с таким id отсутствует ☹')


@current_action_save
def participate_tournament(chat_id=None, select_item_id=None, message=None):
    tp = models.TelegramProfile.objects.get(chat_id=chat_id)

    if tp.nick_name is None:
        data_list = [['Мои данные 😎', 'pac_about'],]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)

        settings.BOT.send_message(chat_id, models.Message.objects.filter(message_type=models.USER_IS_NOT_WRITE_ABOUT).first().text, reply_markup=reply_markup, parse_mode='Markdown')
        return True

    tournament = models.Tournament.objects.get(id=select_item_id)

    if tournament.user:
        data_list = [['Написать 💬', 'write_message_to:{}'.format(tournament.user.chat_id)],
                 ['Добавить в друзья', 'friend_confirmation:{}'.format(tournament.user.id)]]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)



    if models.MyTournament.objects.filter(own=chat_id, tournament_id=select_item_id).count() > 0:
        if tournament.user:
            settings.BOT.send_message(chat_id,
                                      'Вы уже участвуете в данном турнире\nПрофиль организатора\n{}'.format(
                                          helpers.make_user_info(tournament.user)),
                                      reply_markup=reply_markup)
        else:
            settings.BOT.send_message(chat_id, 'Вы уже участвуете в данном турнире')
    else:
        data_list = [['Написать 💬', 'write_message_to:{}'.format(chat_id)],
                     ['Добавить в друзья', 'friend_confirmation:{}'.format(chat_id)]]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)

        if tournament.user:
            settings.BOT.send_message(tournament.user.chat_id,
                                      'Присоеденился к турниру\n{}'.format(helpers.make_user_info(tp)),
                                      reply_markup=reply_markup)
            models.MyTournament.objects.create(own=chat_id, tournament_id=select_item_id)

            settings.BOT.send_message(chat_id, 'Турнир сохранен\nПрофиль организатора\n{}'.format(helpers.make_user_info(tournament.user)),
                                      reply_markup=reply_markup)
        else:
            models.MyTournament.objects.create(own=chat_id, tournament_id=select_item_id)
            settings.BOT.send_message(chat_id, 'Турнир сохранен')


@current_action_save
def my_tournaments(chat_id=None, select_item_id=None, message=None):
    tournament_ids = [tournament.tournament_id for tournament in models.MyTournament.objects.filter(own=chat_id)]

    tournament_ids += [t.id for t in models.Tournament.objects.filter(user__chat_id=chat_id)]

    messages = []

    if models.Tournament.objects.filter(id__in=tournament_ids).count() == 0:
        settings.BOT.send_message(chat_id, 'У вас пока отсутствуют записи на турниры☹')
    else:
        for tournament in models.Tournament.objects.filter(id__in=tournament_ids):
            file = open(tournament.img.path, 'rb')

            data_list = [
                ['Cyber-клуб 👾', 'show_club:{}'.format(tournament.comp_club.id)],
                ['Участники 👥', 'show_tournament_info:{}'.format(tournament.id)],
                #['❤ ({})'.format(tournament.likes), 'like_tournament:{}'.format(tournament.id)],
            ]

            if tournament.user:
                if tournament.user.chat_id != chat_id:
                    data_list.append(['Удалить турнир ❌', 'delete_tournament:{}'.format(tournament.id)])
                else:
                    data_list.append(['Отменить участие ❌', 'cancepart_tournament:{}'.format(tournament.id)])
            else:
                data_list.append(['Отменить участие ❌', 'cancepart_tournament:{}'.format(tournament.id)])

            reply_markup = helpers.keyboard_creator_from_data_list(data_list, new_count=2, oneline=True)

            messages.append(
                [helpers.message_creator(tournament, ['title', ], 'promotion_scheme'), file, reply_markup])

        after_thred = None
        settings.BOT.send_message(chat_id, models.Message.objects.filter(message_type=models.MESSAGE_PERSONAL_AREA_MY_TOURNAMENTS_BTN).first().text, parse_mode='Markdown')
        send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages, after_thred=after_thred)
        send_messages_thread.start()


@current_action_save
def add_duel(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    last_select_values = ast.literal_eval(telegram_profile.last_select_values)

    messages = []


    comp_club_id = last_select_values.get('select_item_id')

    data_list = [['Обычная дуэль ⚔️', 'add_ordinary_duel:{}'.format(comp_club_id)],
                 ['Поиск тиммейтов 👥', 'add_duel_withsearchteas:{}'.format(comp_club_id)]]
    reply_markup = helpers.keyboard_creator_from_data_list(data_list)

    messages.append(
        [models.Message.objects.filter(message_type=models.MESSAGE_DUEL_5).first().text, reply_markup])

    send_messages_thread = work_treads.SendMessagesThread(chat_id, messages, 1)
    send_messages_thread.start()


@current_action_save
def add_ordinary_duel(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id, 'Введите описание дуэли, вид игры, '
                                       'количество игроков, приз и иную необходимую информацию')


@current_action_save
def add_ordinary_duel_save(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    last_select_values = ast.literal_eval(telegram_profile.last_select_values)


    models.Duel.objects.create(title=message.text, comp_club_id=last_select_values.get('select_item_id'),
                               user_id=telegram_profile.id, create_date=datetime.datetime.now())
    settings.BOT.send_message(chat_id, models.Message.objects.filter(message_type=models.MESSAGE_DUEL_6).first().text, parse_mode='Markdown')


@current_action_save
def take_challenge(chat_id=None, select_item_id=None, message=None):
    try:
        models.MyDuel.objects.get(own=chat_id, duel_id=select_item_id)
        duel = models.Duel.objects.get(id=select_item_id)

        if duel.number_participants:
            participant_ids = [duel.user.chat_id]

            participant_ids += [my_duel.own for my_duel in models.MyDuel.objects.filter(duel_id=duel.id)]

            reply_markup = telebot.types.InlineKeyboardMarkup()

            for participant_id in participant_ids:
                chat = settings.BOT.get_chat(participant_id)
                user_name_to_btn = helpers.make_user_name(chat)

                btn_list = [telebot.types.InlineKeyboardButton(text=user_name_to_btn,
                                                               callback_data='user_info:{}'.format(
                                                                   participant_id))]

                reply_markup.add(*btn_list)


            btn_list = [telebot.types.InlineKeyboardButton(text='Написать 💬',
                                                           callback_data='duel_write_message_all:{}'.format(
                                                               duel.id)),
                        telebot.types.InlineKeyboardButton(text='Друзья 👥',
                                                           callback_data='friend_confirmation:{}'.format(
                                                               duel.user.id)),
                        ]
            reply_markup.add(*btn_list)

            # creator_user_info = helpers.make_user_info(duel.user)

            for participant_id in participant_ids:
                print(participant_id)
                try:
                    settings.BOT.send_message(participant_id, 'Участники 👥', reply_markup=reply_markup)
                except ApiException as e:
                    print(e)

    except ObjectDoesNotExist:
        #telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)


        duel = models.Duel.objects.get(id=select_item_id)

        if str(duel.user.chat_id) != str(chat_id):
            models.MyDuel.objects.create(own=chat_id, duel_id=select_item_id)

        if duel.number_participants:
            participant_ids = [duel.user.chat_id]

            participant_ids += [my_duel.own for my_duel in models.MyDuel.objects.filter(duel_id=duel.id)]

            reply_markup = telebot.types.InlineKeyboardMarkup()

            for participant_id in participant_ids:
                chat = settings.BOT.get_chat(participant_id)
                user_name_to_btn = helpers.make_user_name(chat)



                btn_list = [telebot.types.InlineKeyboardButton(text=user_name_to_btn,
                                                               callback_data='user_info:{}'.format(
                                                               participant_id))]
                reply_markup.add(*btn_list)

            btn_list = [telebot.types.InlineKeyboardButton(text='Написать 💬',
                                                           callback_data='duel_write_message_all:{}'.format(
                                                               duel.id)),
                        telebot.types.InlineKeyboardButton(text='Друзья 👥',
                                                           callback_data='friend_confirmation:{}'.format(
                                                               duel.user.id)),
                        ]

            reply_markup.add(*btn_list)

            #creator_user_info = helpers.make_user_info(duel.user)

            for participant_id in participant_ids:
                try:
                    settings.BOT.send_message(participant_id, 'Участники 👥', reply_markup=reply_markup)
                except ApiException as e:
                    print(e)
        else:
            chat = settings.BOT.get_chat(duel.user.chat_id)
            user_name_to_btn = helpers.make_user_name(chat)

            reply_markup = telebot.types.InlineKeyboardMarkup()

            btn_list = [telebot.types.InlineKeyboardButton(text='Написать 💬',
                                                           callback_data='duel_write_message_all:{}'.format(
                                                               duel.id)),
                        telebot.types.InlineKeyboardButton(text='Друзья 👥',
                                                           callback_data='friend_confirmation:{}'.format(
                                                               duel.user.id)),
                        ]


            reply_markup.add(*btn_list)

            creator_user_info = helpers.make_user_info(duel.user)
            settings.BOT.send_message(chat_id, 'Вызов принят\n{}'.format(creator_user_info), reply_markup=reply_markup)
            duel = models.Duel.objects.get(id=select_item_id)

            chat = settings.BOT.get_chat(chat_id)
            #user_name_to_btn = helpers.make_user_name(chat)

            reply_markup = telebot.types.InlineKeyboardMarkup()
            btn_list = [telebot.types.InlineKeyboardButton(text='Написать 💬',
                                                           callback_data='write_message_to_duel_author:{}'.format(chat.id)),
                        telebot.types.InlineKeyboardButton(text='Друзья 👥',
                                                           callback_data='friend_confirmation:{}'.format(
                                                               duel.user.id))
                        ]
            reply_markup.add(*btn_list)


            profile = models.TelegramProfile.objects.get(chat_id=chat_id)

            accepted_call_info = helpers.make_user_info(profile)

            author_info = '{} \nпринял вызов'.format(accepted_call_info)

            settings.BOT.send_message(duel.user.chat_id, author_info,
                                      reply_markup=reply_markup)





@current_action_save
def pac_myduels(chat_id=None, select_item_id=None, message=None):
    my_duel_ids = [my_duel.duel_id for my_duel in models.MyDuel.objects.filter(own=chat_id)]

    i_creator_duel_ids = [duel.id for duel in models.Duel.objects.filter(user__chat_id=chat_id)]

    duel_ids = list(set((my_duel_ids + i_creator_duel_ids)))

    messages = []

    if models.Duel.objects.filter(id__in=duel_ids).count() == 0:
        settings.BOT.send_message(chat_id, 'У вас пока отсутствуют дуэли☹')
        reply_markup = telebot.types.InlineKeyboardMarkup()
        add_messages = []
        add_messages.append([models.Message.objects.filter(message_type=models.MESSAGE_PERSONAL_AREA_MY_DUELS_BTN).first().text, reply_markup])

        after_thred = work_treads.SendMessagesThread(chat_id, add_messages)
    else:
        for duel in models.Duel.objects.filter(id__in=duel_ids):
            if duel.number_participants:
                data_list = [
                    ['Написать сообщение 💬', 'write_m_to_duel_author:{}'.format(duel.user.id)],
                    ['Удалить дуэль ❌', 'del_my_duel:{}'.format(duel.id)]]
            else:
                data_list = [
                    ['Участники 👥', 'duel_participants:{}'.format(duel.id)],
                    ['Удалить дуэль ❌', 'del_my_duel:{}'.format(duel.id)]]
            reply_markup = helpers.keyboard_creator_from_data_list(data_list)

            messages.append(
                [helpers.message_creator(duel, ('id', 'title', 'get_hours', 'get_create_date', 'get_type'), 'duel_scheme'), reply_markup])


        settings.BOT.send_message(chat_id, 'Ваши текущие дуэли. Ваши дуэли пропадут через 24 часа, если никто не примет ваш вызов')
        reply_markup = telebot.types.InlineKeyboardMarkup()
        add_messages = []
        add_messages.append([models.Message.objects.filter(message_type=models.MESSAGE_PERSONAL_AREA_MY_DUELS_BTN).first().text, reply_markup])

        after_thred = work_treads.SendMessagesThread(chat_id, add_messages)
    send_messages_thread = work_treads.SendMessagesThread(chat_id, messages, after_thred=after_thred)
    send_messages_thread.start()


@current_action_save
def del_my_duel(chat_id=None, select_item_id=None, message=None):
    try:
        my_d = models.MyDuel.objects.get(duel_id=select_item_id, own=chat_id)
        my_d.delete()
        settings.BOT.send_message(chat_id, 'Дуэль успешно удалена')
    except ObjectDoesNotExist:
        duel = models.Duel.objects.get(id=select_item_id)
        if str(duel.user.chat_id) == str(chat_id):
            duel.delete()
            settings.BOT.send_message(chat_id, 'Дуэль успешно удалена')


@current_action_save
def write_message_to_duel_author(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(id=select_item_id)

    data_list = [
        ['Добавить в друзья', 'friend_confirmation:{}'.format(telegram_profile.id)]
    ]
    reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)
    settings.BOT.send_message(chat_id, 'Введите текст сообщения', reply_markup=reply_markup)


@current_action_save
def send_message_to_duel_author(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    last_select_values = ast.literal_eval(telegram_profile.last_select_values)
    print(last_select_values, 'LV')

    chat = settings.BOT.get_chat(chat_id)

    user_name_to_btn = helpers.make_user_name(chat)

    reply_markup = telebot.types.InlineKeyboardMarkup()
    btn_list = [telebot.types.InlineKeyboardButton(text='{}'.format(user_name_to_btn),
                                                   callback_data='rewrite_message_to_duel_author:{}'.format(chat_id))]
    reply_markup.add(*btn_list)
    recepient_profile = models.TelegramProfile.objects.get(id=last_select_values.get('select_item_id'))
    settings.BOT.send_message(recepient_profile.chat_id, 'Вам сообщение\n{}'.format(message.text), reply_markup=reply_markup)


@current_action_save
def duel_participants(chat_id=None, select_item_id=None, message=None):
    all_participants_ids = []
    duel = models.Duel.objects.get(id=select_item_id)
    all_participants_ids.append(duel.user.chat_id)

    owner = duel.user.chat_id

    my_duel_participants_ids = [duel.own for duel in models.MyDuel.objects.filter(duel_id=select_item_id)]
    all_participants_ids = list(set(all_participants_ids + my_duel_participants_ids))

    if duel.number_participants:
        reply_markup = telebot.types.InlineKeyboardMarkup()
        for profile in models.TelegramProfile.objects.filter(chat_id__in=all_participants_ids):
            if chat_id == int(profile.chat_id):
                continue
            chat = settings.BOT.get_chat(profile.chat_id)
            user_name_to_btn = helpers.make_user_name(chat)

            btn_list = [telebot.types.InlineKeyboardButton(text=user_name_to_btn,
                                                           callback_data='user_info:{}'.format(
                                                               profile.id))]

            reply_markup.add(*btn_list)

        btn_list = [telebot.types.InlineKeyboardButton(text='Написать 💬',
                                                       callback_data='duel_write_message_all:{}'.format(
                                                           duel.id))]
        reply_markup.add(*btn_list)
        settings.BOT.send_message(chat_id, '{}: {}'.format('Создатель' if profile.chat_id == owner else 'Участник',
                                                           user_name_to_btn),
                                  reply_markup=reply_markup)


    else:
        for profile in models.TelegramProfile.objects.filter(chat_id__in=all_participants_ids):
            if chat_id == int(profile.chat_id):
                continue

            chat = settings.BOT.get_chat(profile.chat_id)



            user_name_to_btn = helpers.make_user_name(chat)

            reply_markup = telebot.types.InlineKeyboardMarkup()
            btn_list = [telebot.types.InlineKeyboardButton(text='Написать сообщение 💬',
                                                       callback_data='user_info:{}'.format(
                                                           profile.chat_id))]
            reply_markup.add(*btn_list)
            settings.BOT.send_message(chat_id, '{}: {}'.format('Создатель' if profile.chat_id == owner else 'Участник' ,user_name_to_btn),
                                  reply_markup=reply_markup)


@current_action_save
def send_message_to_duel_user(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    last_select_values = ast.literal_eval(telegram_profile.last_select_values)

    #chat = settings.BOT.get_chat(chat_id)

    #user_name_to_btn = helpers.make_user_name(chat)

    reply_markup = telebot.types.InlineKeyboardMarkup()
    btn_list = [telebot.types.InlineKeyboardButton(text='Написать 💬',
                                                   callback_data='write_message_to_duel_author:{}'.format(
                                                       chat_id))]
    reply_markup.add(*btn_list)

    recepient_profile = models.TelegramProfile.objects.get(id=last_select_values.get('select_item_id'))
    user_info = helpers.make_user_info(telegram_profile)

    settings.BOT.send_message(recepient_profile.chat_id, 'Вам новое сообщение, от: \n{} \nТекст сообщения\n{}'.format(user_info, message.text))

    settings.BOT.send_message(chat_id, 'Сообщение отправлено')



@current_action_save
def add_duel_withsearchteas(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id, models.Message.objects.filter(message_type=models.MESSAGE_DUEL_7).first().text, parse_mode='Markdown')


@current_action_save
def add_duel_withsearchteas_save(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    last_select_values = ast.literal_eval(telegram_profile.last_select_values)

    duel = models.Duel.objects.create(title=message.text, comp_club_id=last_select_values.get('select_item_id'),
                               user_id=telegram_profile.id, create_date=datetime.datetime.now())

    new_last_select_values = dict(select_item_id=duel.id)
    telegram_profile.last_select_values = new_last_select_values
    telegram_profile.save()
    settings.BOT.send_message(chat_id, models.Message.objects.filter(message_type=models.MESSAGE_DUEL_8).first().text, parse_mode='Markdown')


@current_action_save
def add_duel_withsearchteas_add_count(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    last_select_values = ast.literal_eval(telegram_profile.last_select_values)

    duel = models.Duel.objects.get(id=last_select_values.get('select_item_id'))

    duel.number_participants = int(message.text)
    duel.save()
    settings.BOT.send_message(chat_id, models.Message.objects.filter(message_type=models.MESSAGE_DUEL_9).first().text, parse_mode='Markdown')


@current_action_save
def user_info(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=select_item_id)
    user_info = helpers.make_user_info(telegram_profile)

    data_list = [['Сообщение', 'write_message_to_duel_author:{}'.format(telegram_profile.id)],
                 ['Добавить в друзья', 'friend_confirmation:{}'.format(telegram_profile.id)]]
    reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)

    settings.BOT.send_message(chat_id, user_info, reply_markup=reply_markup)


@current_action_save
def user_info_from_profile(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=select_item_id)
    user_info = helpers.make_user_info(telegram_profile)

    data_list = [['Сообщение', 'write_message_to_duel_author:{}'.format(telegram_profile.id)],
                 ['Удалить из друзей', 'friend_del:{}'.format(telegram_profile.chat_id)]]
    reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)

    settings.BOT.send_message(chat_id, user_info, reply_markup=reply_markup)


@current_action_save
def duel_write_message_all(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id, 'Напишите текст сообщения')

@current_action_save
def duel_send_message_all(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    last_select_values = helpers.get_last_values(telegram_profile.last_select_values)
    duel = models.Duel.objects.get(id=last_select_values.get('select_item_id'))

    user_info = helpers.make_user_info(telegram_profile)

    ids = [duel.user.chat_id]
    ids += [my_du.own for my_du in models.MyDuel.objects.filter(duel_id=duel.id)]

    for id in ids:
        try:
            settings.BOT.send_message(id, 'Дуэль № {}\n{}\nСообщение\n{}'.format(duel.id, user_info, message.text))
        except ApiException as e:
            print(e)


@current_action_save
def show_tournament_info(chat_id=None, select_item_id=None, message=None):
    tournament = models.Tournament.objects.get(id=select_item_id)
    print(tournament)

    participant_ids = [myt.own for myt in models.MyTournament.objects.filter(tournament_id=select_item_id) if str(myt.own) != str(chat_id)]
    reply_markup = telebot.types.InlineKeyboardMarkup()

    btn_list = []

    for participant_id in participant_ids:
        chat = settings.BOT.get_chat(participant_id)
        user_name_to_btn = helpers.make_user_name(chat)

        btn_list.append(telebot.types.InlineKeyboardButton(text=user_name_to_btn,
                                                       callback_data='user_info:{}'.format(
                                                           participant_id)))

    print('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$')
    print(btn_list)
    print('#############################################')
    reply_markup.add(*btn_list)

    if tournament.user:
        if tournament.user.chat_id == chat_id:
            btn_list = [telebot.types.InlineKeyboardButton(text='Написать 💬',
                                                   callback_data='tournament_write_message_all:{}'.format(
                                                       tournament.id))]
            reply_markup.add(*btn_list)


    settings.BOT.send_message(chat_id, 'Участники 👥', reply_markup=reply_markup)


@current_action_save
def tournament_write_message_all(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id, 'Напишите текст сообщения')

@current_action_save
def tournament_send_message_all(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    last_select_values = helpers.get_last_values(telegram_profile.last_select_values)
    tournament = models.Tournament.objects.get(id=last_select_values.get('select_item_id'))

    user_info = helpers.make_user_info(telegram_profile)

    ids = [my_tu.own for my_tu in models.MyTournament.objects.filter(tournament_id=tournament.id)]

    for id in ids:
        try:
            settings.BOT.send_message(id, 'Турнир № {}\n{}\nСообщение\n{}'.format(tournament.id, user_info, message.text))
        except ApiException as e:
            print(e)


@current_action_save
def friend_del(chat_id=None, select_item_id=None, message=None):
    friend = models.Friend.objects.get(own=chat_id, friend=select_item_id)
    friend.delete()

    settings.BOT.send_message(chat_id, 'Успешно удален')


@current_action_save
def tours_clubs(chat_id=None, select_item_id=None, message=None):
    messages = []

    start = 0

    end = start + 20

    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

    if telegram_profile.selected_sity:
        selected_sity = models.City.objects.get(title=telegram_profile.selected_sity)
    else:
        selected_sity = models.City.objects.get(title='москва')
    selected_sity

    club_ids = [city.id for city in models.CompClub.objects.filter(city_id=selected_sity.id)]

    if models.Tournament.objects.filter(comp_club_id__in=club_ids).order_by('-likes').count() == 0:
        settings.BOT.send_message(chat_id, 'Турниры клубов отсутствуют☹')
        return True

    for tournament in models.Tournament.objects.filter(comp_club_id__in=club_ids).order_by('-likes')[start:end]:
        file = open(tournament.img.path, 'rb')

        data_list = [
            ['Cyber-клуб 👾', 'show_club:{}'.format(tournament.comp_club.id)],
            ['Участвовать 💯', 'participate_tournament:{}'.format(tournament.id)]
        ]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)

        messages.append(
            [helpers.message_creator(tournament, ['title', ], 'promotion_scheme'), file, reply_markup])

    after_thred = None

    if models.Tournament.objects.all().count() > end:
        add_messages = []

        data_list = ['Показать еще 🌀', 'tours_clubs_next:{}'.format(end)]
        reply_markup = helpers.keyboard_creator_to_promotion(data_list)

        add_messages.append([models.Message.objects.filter(message_type=models.MESSAGE_TOURNAMENT_2).first().text, reply_markup])


        after_thred = work_treads.SendMessagesThread(chat_id, add_messages)

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages, after_thred=after_thred)
    send_messages_thread.start()


@current_action_save
def tours_clubs_next(chat_id=None, select_item_id=None, message=None):
    messages = []

    start = int(select_item_id)

    end = start + 20

    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

    if telegram_profile.selected_sity:
        selected_sity = models.City.objects.get(title=telegram_profile.selected_sity)
    else:
        selected_sity = models.City.objects.get(title='москва')
    selected_sity

    club_ids = [city.id for city in models.CompClub.objects.filter(city_id=selected_sity.id)]

    if models.Tournament.objects.filter(comp_club_id__in=club_ids).order_by('-likes')[start:end].count() == 0:
        settings.BOT.send_message(chat_id, 'Турниры клубов отсутствуют☹')
        return True

    for tournament in models.Tournament.objects.filter(comp_club_id__in=club_ids).order_by('-likes')[start:end]:
        file = open(tournament.img.path, 'rb')

        data_list = [
            ['Cyber-клуб 👾', 'show_club:{}'.format(tournament.comp_club.id)],
            ['Участвовать 💯', 'participate_tournament:{}'.format(tournament.id)]
        ]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)

        messages.append(
            [helpers.message_creator(tournament, ['title', ], 'promotion_scheme'), file, reply_markup])

    after_thred = None

    if models.Tournament.objects.filter(comp_club_id__in=club_ids).order_by('-likes')[start:end].count() > end:
        add_messages = []

        data_list = ['Показать еще 🌀', 'tours_clubs_next:{}'.format(end)]
        reply_markup = helpers.keyboard_creator_to_promotion(data_list)

        add_messages.append(['Еще турниры', reply_markup])

        #if

        after_thred = work_treads.SendMessagesThread(chat_id, add_messages)

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages, after_thred=after_thred)
    send_messages_thread.start()


@current_action_save
def privat_tours(chat_id=None, select_item_id=None, message=None):
    messages = []

    start = 0

    end = start + 20

    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

    if telegram_profile.selected_sity:
        selected_sity = models.City.objects.get(title=telegram_profile.selected_sity)
    else:
        selected_sity = models.City.objects.get(title='москва')


    club_ids = [city.id for city in models.CompClub.objects.filter(city_id=selected_sity.id)]

    if models.Tournament.objects.filter(comp_club_id__in=club_ids).exclude(user__isnull=True).order_by('-likes').count() == 0:
        settings.BOT.send_message(chat_id, 'Частных турниров нет ☹')
        return True

    for tournament in models.Tournament.objects.filter(comp_club_id__in=club_ids).exclude(user__isnull=True).order_by('-likes')[start:end]:
        file = open(tournament.img.path, 'rb')

        data_list = [
            ['Cyber-клуб 👾', 'show_club:{}'.format(tournament.comp_club.id)],
            ['Отменить участие ❌', 'cancepart_tournament:{}'.format(tournament.id)]
        ]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)

        messages.append(
            [helpers.message_creator(tournament, ['title', ], 'promotion_scheme'), file, reply_markup])

    after_thred = None

    if models.Tournament.objects.filter(comp_club_id__in=club_ids).order_by('-likes')[start:end].count() > end:
        add_messages = []

        data_list = ['Показать еще 🌀', 'privat_tours_next:{}'.format(end)]
        reply_markup = helpers.keyboard_creator_to_promotion(data_list)

        add_messages.append([models.Message.objects.filter(message_type=models.MESSAGE_TOURNAMENT_3).first().text, reply_markup])

        after_thred = work_treads.SendMessagesThread(chat_id, add_messages)

    else:
        add_messages = []

        reply_markup = telebot.types.InlineKeyboardMarkup()

        add_messages.append(
            [models.Message.objects.filter(message_type=models.MESSAGE_TOURNAMENT_3).first().text, reply_markup])

        after_thred = work_treads.SendMessagesThread(chat_id, add_messages)

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages, after_thred=after_thred)
    send_messages_thread.start()


@current_action_save
def privat_tours_next(chat_id=None, select_item_id=None, message=None):
    messages = []

    start = int(select_item_id)

    end = start + 20

    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

    if telegram_profile.selected_sity:
        selected_sity = models.City.objects.get(title=telegram_profile.selected_sity)
    else:
        selected_sity = models.City.objects.get(title='москва')
    selected_sity

    club_ids = [city.id for city in models.CompClub.objects.filter(city_id=selected_sity.id)]

    if models.Tournament.objects.filter(comp_club_id__in=club_ids).exclude(user__isnull=True).order_by('-likes').count() == 0:
        settings.BOT.send_message(chat_id, 'Частных турниров нет ☹')
        return True

    for tournament in models.Tournament.objects.filter(comp_club_id__in=club_ids).exclude(user__isnull=True).order_by('-likes')[start:end]:
        file = open(tournament.img.path, 'rb')

        data_list = [
            ['Cyber-клуб 👾', 'show_club:{}'.format(tournament.comp_club.id)],
            ['Отменить участие ❌', 'cancepart_tournament:{}'.format(tournament.id)]
        ]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)

        messages.append(
            [helpers.message_creator(tournament, ['title', ], 'promotion_scheme'), file, reply_markup])

    after_thred = None

    if models.Tournament.objects.filter(comp_club_id__in=club_ids).order_by('-likes')[start:end].count() > end:
        add_messages = []

        data_list = ['Показать еще 🌀', 'privat_tours_next:{}'.format(end)]
        reply_markup = helpers.keyboard_creator_to_promotion(data_list)

        add_messages.append([models.Message.objects.filter(message_type=models.MESSAGE_TOURNAMENT_3).first().text, reply_markup])

        after_thred = work_treads.SendMessagesThread(chat_id, add_messages)

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages, after_thred=after_thred)
    send_messages_thread.start()


@current_action_save
def tours_listgames(chat_id=None, select_item_id=None, message=None):
    games = [game.title for game in models.Game.objects.all()]
    settings.BOT.send_message(chat_id, 'Скопируй хештег игры и отправь боту 👾\n{}'.format('\n'.join(games)))


@current_action_save
def show_club_tournaments(chat_id=None, select_item_id=None, message=None):
    messages = []

    request_value = select_item_id.split(':')


    start = 0

    if len(request_value) > 1:
        print('LEN', int(request_value[1]))
        start = int(request_value[1])
        comp_club_id = request_value[0]
    else:
        comp_club_id = select_item_id
    end = start + 20


    for tournament in models.Tournament.objects.filter(comp_club_id=comp_club_id, user__isnull=True).order_by('-likes')[
                      start:end]:
        file = open(tournament.img.path, 'rb')

        data_list = [
            ['Показать клуб', 'show_club:{}'.format(tournament.comp_club.id)],
            # ['❤ ({})'.format(tournament.likes), 'like_tournament:{}'.format(tournament.id)],
            ['Участвовать 💯', 'participate_tournament:{}'.format(tournament.id)]
        ]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)

        messages.append(
            [helpers.message_creator(tournament, ['title', ], 'promotion_scheme'), file, reply_markup])

    after_thred = None

    if models.Tournament.objects.filter(comp_club_id=comp_club_id).count() > end:
        add_messages = []

        data_list = ['Показать еще', 'actions:{}:{}'.format(tournament.comp_club.id, end)]
        reply_markup = helpers.keyboard_creator_to_promotion(data_list)

        add_messages.append(['Еще турниры', reply_markup])

        after_thred = work_treads.SendMessagesThread(chat_id, add_messages)

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages, after_thred=after_thred)
    send_messages_thread.start()


@current_action_save
def show_user_tournaments(chat_id=None, select_item_id=None, message=None):
    messages = []

    request_value = select_item_id.split(':')

    start = 0

    if len(request_value) > 1:
        print('LEN', int(request_value[1]))
        start = int(request_value[1])
        comp_club_id = request_value[0]
    else:
        comp_club_id = select_item_id
    end = start + 20


    if models.Tournament.objects.filter(comp_club_id=comp_club_id, user__isnull=False).count() == 0:
        data_list = [
            ['Добавить Ч-турнир?', 'user_create_tournament:{}'.format(comp_club_id)]
        ]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list)
        settings.BOT.send_message(chat_id, 'Частных турниров нет ☹', reply_markup=reply_markup)
        return {}


    for tournament in models.Tournament.objects.filter(comp_club_id=comp_club_id, user__isnull=False).order_by('-likes')[
                      start:end]:
        file = open(tournament.img.path, 'rb')

        data_list = [
            ['Показать клуб', 'show_club:{}'.format(tournament.comp_club.id)],
            ['Участников {}'.format(models.MyTournament.objects.filter(tournament_id=tournament.id).count()),
             'user_tourn_participants:{}'.format(tournament.id)],
            ['Участвовать 💯', 'participate_tournament:{}'.format(tournament.id)]
        ]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)

        messages.append(
            [helpers.message_creator(tournament, ('id', 'title', 'create_date'), 'tournament_scheme_user'), file, reply_markup])

    after_thred = None

    if models.Tournament.objects.filter(comp_club_id=comp_club_id).count() > end:
        add_messages = []

        data_list = ['Показать еще', 'actions:{}:{}'.format(tournament.comp_club.id, end)]
        reply_markup = helpers.keyboard_creator_to_promotion(data_list)

        add_messages.append(['Еще турниры', reply_markup])

        after_thred = work_treads.SendMessagesThread(chat_id, add_messages)

    send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages, after_thred=after_thred)
    send_messages_thread.start()



@current_action_save
def user_create_tournament(chat_id=None, select_item_id=None, message=None):
    data_list = [
        ['Отправить заявку 🗿', 'send_request:{}'.format(select_item_id)]
    ]
    reply_markup = helpers.keyboard_creator_from_data_list(data_list)
    settings.BOT.send_message(chat_id, models.Message.objects.filter(message_type=models.MESSAGE_3).first().text, reply_markup=reply_markup, parse_mode="Markdown")
    #photo = open('/home/comp_club/user_create_tournament.png', 'rb')
    #settings.BOT.send_photo(chat_id, photo, 'Пришлите картинку с описанием турнира')


@current_action_save
def user_create_tournament_save(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    last_select_values = ast.literal_eval(telegram_profile.last_select_values)

    comp_club_id = last_select_values.get('select_item_id')

    comp_club = models.CompClub.objects.get(id=comp_club_id)

    fileID = message.photo[-1].file_id
    file_info = settings.BOT.get_file(fileID)
    downloaded_file = settings.BOT.download_file(file_info.file_path)

    file_full_path = 'https://api.telegram.org/file/bot{}/{}'.format(settings.BOT.token, file_info.file_path)


    r = requests.get(file_full_path)

    with open("image.jpg", 'wb') as f:
        f.write(r.content)

    reopen = open('image.jpg', 'rb')
    django_file = File(reopen)


    tournament = models.Tournament.objects.create(comp_club=comp_club, title=message.caption, img=django_file,
                                                  user=telegram_profile)

    settings.BOT.send_message(chat_id, models.Message.objects.filter(message_type=models.MESSAGE_5).first().text, parse_mode="Markdown")

    messages = []

    file = open(tournament.img.path, 'rb')

    messages.append(
        [helpers.message_creator(tournament, ['title', ], 'promotion_scheme'), file, None])

    for favorite in comp_club.club_favorites.all():
        client = models.TelegramProfile.objects.get(chat_id=favorite.chat_id)
        if client.notification_tournament:
            send_messages_thread = work_treads.SendPhotoMessagesThread(chat_id, messages)
            send_messages_thread.start()


@current_action_save
def write_message_to(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id, 'Введите текст сообщения')


@current_action_save
def send_message_to(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    last_select_values = ast.literal_eval(telegram_profile.last_select_values)

    chat = settings.BOT.get_chat(chat_id)
    user_name_to_btn = helpers.make_user_name(chat)

    recepient_id = last_select_values.get('select_item_id')

    data_list = [['Написать 💬', 'write_message_to:{}'.format(chat_id)],
                 ['Добавить в друзья', 'friend_confirmation:{}'.format(chat_id)]]
    reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)
    settings.BOT.send_message(recepient_id, 'Сообщение от {}\n{}'.format(user_name_to_btn, message.text), reply_markup=reply_markup)


@current_action_save
def user_tourn_participants(chat_id=None, select_item_id=None, message=None):
    ids = [mt.own for mt in models.MyTournament.objects.filter(tournament_id=select_item_id)]

    reply_markup = telebot.types.InlineKeyboardMarkup()
    tournament = models.Tournament.objects.get(id=select_item_id)

    for profile in models.TelegramProfile.objects.filter(chat_id__in=ids):
        if chat_id == int(profile.chat_id):
            continue
        chat = settings.BOT.get_chat(profile.chat_id)
        user_name_to_btn = helpers.make_user_name(chat)

        btn_list = [telebot.types.InlineKeyboardButton(text=user_name_to_btn,
                                                       callback_data='user_info:{}'.format(
                                                           profile.id))]

        reply_markup.add(*btn_list)

        data_list = [['Написать 💬', 'write_message_to:{}'.format(profile.chat_id)],
                 ['Добавить в друзья', 'friend_confirmation:{}'.format(profile.chat_id)]]
        reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)

        settings.BOT.send_message(chat_id, '{}: {}'.format('Организатор' if profile.chat_id == tournament.user.chat_id else 'Участник',
                                                       user_name_to_btn),
                              reply_markup=reply_markup)


@current_action_save
def cancepart_tournament(chat_id=None, select_item_id=None, message=None):
    mt = models.MyTournament.objects.get(tournament_id=select_item_id, own=chat_id)
    mt.delete()
    settings.BOT.send_message(chat_id, 'Участее отменено')


@current_action_save
def become_admin(chat_id=None, select_item_id=None, message=None):
    print(message.text)

    comp_club = models.CompClub.objects.get(club_id=message.text[1:])
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)
    print(comp_club)

    password = get_random_string(length=32)

    print(password)

    if telegram_profile.user:
        telegram_profile.user.set_password(password)
        telegram_profile.user.save()
    else:
        print('New User')
        user = models.User.objects.create_user(chat_id, '{}@telega.com'.format(chat_id), password, is_staff=True)
        telegram_profile.user = user
        telegram_profile.save()

        comp_club.employees.add(telegram_profile)
        comp_club.save()



    reply_markup = telebot.types.InlineKeyboardMarkup()

    btn_list = [telebot.types.InlineKeyboardButton(text='Авторизируйтесь',
                                                       url='http://139.162.172.102:8080/admin/login/?next=/')]
    reply_markup.add(*btn_list)
    settings.BOT.send_message(chat_id,
                                  'Вы стали админом\nВаш логин: {}\nВаш пароль: {}'.format(chat_id, password),
                                  reply_markup=reply_markup)



@current_action_save
def donate(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id, '💰 помощь - 17ezdeGovn5PNudnDFxJMBYGzjypCNzKLP Bitcoin \n💡помощь - @F2F_Official')


@current_action_save
def btn_about_s(chat_id=None, select_item_id=None, message=None):
    service_instruction(chat_id, select_item_id, message)

@current_action_save
def send_request(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id, models.Message.objects.filter(message_type=models.MESSAGE_4).first().text, parse_mode="Markdown")


@current_action_save
def del_clubfavorite(chat_id=None, select_item_id=None, message=None):
    fc = models.Favorite.objects.get(chat_id=chat_id, club_id=select_item_id)
    fc.delete()
    settings.BOT.send_message(chat_id, 'Клуб удален из избранного')


@current_action_save
def looking_for_clubs(chat_id=None, select_item_id=None, message=None):
    telegram_profile = models.TelegramProfile.objects.get(chat_id=chat_id)

    selected_sity = models.City.objects.get(title=telegram_profile.selected_sity)
    comp_clubs = models.CompClub.objects.filter(city_id=selected_sity.id).count()

    data_list = [['Все клубы 👾 {}'.format(comp_clubs), 'all_clubs:1'],
                 ['Умный поиск 🧠', 'smart_search:1']]
    reply_markup = helpers.keyboard_creator_from_data_list(data_list, oneline=True)
    settings.BOT.send_message(chat_id, models.Message.objects.filter(message_type=models.MESSAGE_CYBER_CLUB_1).first().text,
                              reply_markup=reply_markup, parse_mode="Markdown")


@current_action_save
def smart_search(chat_id=None, select_item_id=None, message=None):
    settings.BOT.send_message(chat_id,
                              models.Message.objects.filter(message_type=models.MESSAGE_CYBER_CLUB_2).first().text, parse_mode="Markdown")


@current_action_save
def halls(chat_id=None, select_item_id=None, message=None):
    data_list = []

    text = ''


    for hall in models.Hall.objects.filter(comp_club_id=select_item_id):
        text += '{}\n {}\n{}\n'.format(hall.title, hall.get_prices(), hall.get_equipments())
        data_list.append([hall.title, 'select_hall:{}'.format(hall.id)])

    reply_markup = helpers.keyboard_creator_from_data_list(data_list)
    settings.BOT.send_message(chat_id, text, reply_markup=reply_markup)


@current_action_save
def select_hall(chat_id=None, select_item_id=None, message=None):
    hall = models.Hall.objects.get(id=select_item_id)


    bot_keyboard = telebot.types.InlineKeyboardMarkup(row_width=2)




    btn_list = [telebot.types.InlineKeyboardButton(text='{}'.format(price.get_price_info()),
                                                   callback_data='start_reservation:{}'.format(price.id)) for price in hall.hall_prices.all()]


    bot_keyboard.add(*btn_list)



    settings.BOT.send_message(message.from_user.id, 'Выберете тариф', reply_markup=bot_keyboard)


title_months = {
    1: 'Январь',
    2: 'Феврать',
    3: 'Март',
    4: 'Апрель',
    5: 'Май',
    6: 'Июнь',
    7: 'Июль',
    8: 'Август',
    9: 'Сентябрь',
    10: 'Октябрь',
    11: 'Ноябрь',
    12: 'Декабрь'
}

@current_action_save
def start_reservation(chat_id=None, select_item_id=None, message=None):
    price = models.Price.objects.get(id=select_item_id)

    reply_markup = telebot.types.InlineKeyboardMarkup(row_width=3)
    btn_list = []
    from datetime import timedelta

    timedelta(hours=24)

    current_data = datetime.datetime.now()
    btn_list.append(
        telebot.types.InlineKeyboardButton(text='Сегодня',
                                           callback_data='reservation_start_time:{}'.format(str(current_data.date()))))

    for i in range(1, 15):
        next_d = current_data + timedelta(days=i)
        if i == 1:
            btn_list.append(telebot.types.InlineKeyboardButton(text='Завтра',
                                                           callback_data='reservation_start_time:{}'.format(
                                                               str(current_data.date()))))
        else:
            btn_list.append(telebot.types.InlineKeyboardButton(text='{} {}'.format(next_d.date().day, title_months.get(next_d.date().month)),
                                                               callback_data='reservation_start_time:{}'.format(
                                                                   str(current_data.date()))))

    reply_markup.add(*btn_list)

    if models.Reservation.objects.filter(reservation_date=None, chat_id=chat_id).count() > 0:

        for r in models.Reservation.objects.filter(reservation_date=None, chat_id=chat_id):
            r.delete()

        models.Reservation.objects.create(chat_id=chat_id, price=price)

        settings.BOT.send_message(message.from_user.id, 'Выберете желаемую дату бронирования', reply_markup=reply_markup)
    else:
        models.Reservation.objects.create(chat_id=chat_id, price=price)
        settings.BOT.send_message(message.from_user.id, 'Выберете желаемую дату бронирования', reply_markup=reply_markup)


@current_action_save
def select_reservation_date(chat_id=None, select_item_id=None, message=None):
    reply_markup = telebot.types.InlineKeyboardMarkup(row_width=3)
    btn_list = []
    from datetime import timedelta

    timedelta(hours=24)

    current_data = datetime.datetime.now()
    btn_list.append(
        telebot.types.InlineKeyboardButton(text=str(current_data.date()), callback_data='reservation_start_time:{}'.format(str(current_data.date()))))

    for i in range(1, 15):
        next_d = current_data + timedelta(days=i)
        btn_list.append(telebot.types.InlineKeyboardButton(text=str(next_d.date()), callback_data='reservation_start_time:{}'.format(str(current_data.date()))))

    reply_markup.add(*btn_list)

    settings.BOT.send_message(message.from_user.id, 'Выберете желаемую дату бронирования', reply_markup=reply_markup)


@current_action_save
def reservation_start_time(chat_id=None, select_item_id=None, message=None):
    reservation = models.Reservation.objects.filter(chat_id=chat_id, reservation_date=None).first()

    for reservation in models.Reservation.objects.filter(chat_id=chat_id, reservation_date=None):
        print(select_item_id)
        reservation.reservation_date = select_item_id
        reservation.save()

    if reservation.price.price_type == models.PRICE_TO_PERIOD:
        reservation.reservation_time = reservation.price.start_time
        reservation.reservation_end_time = reservation.price.end_time
        reservation.save()
        select_places_not_save_time(chat_id, select_item_id, message)
        return True


    if reservation.price.hall.comp_club.start_work == 0 and reservation.price.hall.comp_club.end_work == 0:
        reply_markup = telebot.types.InlineKeyboardMarkup(row_width=8)

        btn_list = []

        for h in models.HOUR_list:
            btn_list.append(telebot.types.InlineKeyboardButton(text=str(h), callback_data='reservation_end_time:{}'.format(str(h))))

        reply_markup.add(*btn_list)
        settings.BOT.send_message(message.from_user.id, 'Выберете начальное время бони', reply_markup=reply_markup)

    else:

        reply_markup = telebot.types.InlineKeyboardMarkup(row_width=8)

        btn_list = []

        for h in models.HOUR_list:
            if h >= reservation.price.hall.comp_club.start_work and h <= reservation.price.hall.comp_club.end_work:
                btn_list.append(telebot.types.InlineKeyboardButton(text=str(h), callback_data='reservation_end_time:{}'.format(str(h))))

        reply_markup.add(*btn_list)
        settings.BOT.send_message(message.from_user.id, 'Выберете начальное время бони', reply_markup=reply_markup)



@current_action_save
def reservation_end_time(chat_id=None, select_item_id=None, message=None):
    reservation = models.Reservation.objects.filter(chat_id=chat_id, reservation_time=None).first()


    for reservation in models.Reservation.objects.filter(chat_id=chat_id, reservation_time=None):
        reservation.reservation_time = '{}:00:00'.format('0{}'.format(select_item_id) if int(select_item_id) < 10 else select_item_id)
        reservation.save()

    if reservation.price.price_type == models.PRICE_TO_PACKAGE:
        print('DDDDDDDDDDDDDDDDDDDDDDDDDDDD')
        print(reservation.reservation_time)
        print(type(reservation.reservation_time))
        print('DDDDDDDDDDDDDDDDDDDDDDDDDDDDD')

        dt_obj = datetime.datetime.strptime(reservation.reservation_time, '%H:%M:%S')

        reservation.reservation_end_time = (dt_obj + datetime.timedelta(hours=reservation.price.hours)).time()
        reservation.save()
        select_places_not_save_time(chat_id, select_item_id, message)
        return True

    if reservation.price.hall.comp_club.start_work == 0 and reservation.price.hall.comp_club.end_work == 0:
        reply_markup = telebot.types.InlineKeyboardMarkup(row_width=8)

        btn_list = []

        for h in models.HOUR_list:
            btn_list.append(telebot.types.InlineKeyboardButton(text=str(h), callback_data='select_places:{}'.format(str(h))))

        reply_markup.add(*btn_list)
        settings.BOT.send_message(message.from_user.id, 'Выберете начальное время бони', reply_markup=reply_markup)

    else:

        reply_markup = telebot.types.InlineKeyboardMarkup(row_width=6)

        btn_list = []

        for h in models.HOUR_list:
            if h >= reservation.price.hall.comp_club.start_work and h <= reservation.price.hall.comp_club.end_work:
                btn_list.append(telebot.types.InlineKeyboardButton(text=str(h), callback_data='select_places:{}'.format(str(h))))

        reply_markup.add(*btn_list)
        settings.BOT.send_message(message.from_user.id, 'Выберете конечное время бони', reply_markup=reply_markup)


@current_action_save
def end_reservation(chat_id=None, select_item_id=None, message=None):
    reservation = models.Reservation.objects.filter(chat_id=chat_id, is_booked_client=False).first()


    for reservation in models.Reservation.objects.filter(chat_id=chat_id, is_booked_client=False):
        reservation.reservation_end_time = '{}:00:00'.format('0{}'.format(select_item_id) if int(select_item_id) < 10 else select_item_id)
        reservation.is_booked_client = True
        reservation.save()



    for admin in reservation.playing_place.hall.comp_club.employees.all():
        try:
            settings.BOT.send_message(admin.chat_id, 'Новое бронирование')
        except ApiException as e:
            print(e)

    settings.BOT.send_message(message.from_user.id, 'Бронирование завершено')


def play_place_status(place, chat_id):
    pp_list = [reservation.playing_place.id for reservation in models.Reservation.objects.filter(chat_id=chat_id) if reservation.playing_place]

    if place.in_work:
        if place.id in pp_list:
            return '{}✓'.format(place.title)
        else:
            return '{}'.format(place.title)
    else:
        return '🔴'.format('₇₇')



@current_action_save
def select_places(chat_id=None, select_item_id=None, message=None):
    reservation = models.Reservation.objects.filter(chat_id=chat_id, is_booked_client=False).first()
    reservation.reservation_end_time = '{}:00:00'.format('0{}'.format(select_item_id) if int(select_item_id) < 10 else select_item_id)
    reservation.save()


    bot_keyboard = telebot.types.InlineKeyboardMarkup(row_width=8)

    data_list = reservation.price.hall.get_places

    for ll in data_list:
        btn_list = [telebot.types.InlineKeyboardButton(text='{}'.format(play_place_status(place, chat_id)),
                                                       callback_data='place_selected:{}'.format(place.id)) for place
                    in ll]
        bot_keyboard.add(*btn_list)

    settings.BOT.send_message(message.from_user.id, 'Бронирование', reply_markup=bot_keyboard)



def select_places_edit_message(chat_id=None, select_item_id=None, message=None):
    reservation = models.Reservation.objects.filter(chat_id=chat_id, is_booked_client=False).first()


    bot_keyboard = telebot.types.InlineKeyboardMarkup(row_width=8)

    data_list = reservation.price.hall.get_places

    for ll in data_list:
        btn_list = [telebot.types.InlineKeyboardButton(text='{}'.format(play_place_status(place, chat_id)),
                                                       callback_data='place_selected:{}'.format(place.id)) for place
                    in ll]
        bot_keyboard.add(*btn_list)

    settings.BOT.edit_message_text(message, chat_id=chat_id, message_id=select_item_id)
    settings.BOT.edit_message_reply_markup(chat_id=chat_id, message_id=select_item_id, reply_markup=bot_keyboard)

    #settings.BOT.send_message(message.from_user.id, 'Бронирование', reply_markup=bot_keyboard)


@current_action_save
def select_places_not_save_time(chat_id=None, select_item_id=None, message=None):
    reservation = models.Reservation.objects.filter(chat_id=chat_id, is_booked_client=False).first()
    bot_keyboard = telebot.types.InlineKeyboardMarkup(row_width=8)

    data_list = reservation.price.hall.get_places

    for ll in data_list:
        btn_list = [telebot.types.InlineKeyboardButton(text='{}'.format(' ' if place.in_work else 'x'),
                                                       callback_data='place_selected:{}'.format(place.id)) for place
                    in ll]
        bot_keyboard.add(*btn_list)

    settings.BOT.send_message(message.from_user.id, 'Бронирование', reply_markup=bot_keyboard)


@current_action_save
def place_selected(chat_id=None, select_item_id=None, message=None):
    playing_place = models.PlayingPlace.objects.get(id=select_item_id)
    if playing_place.in_work:

        reply_markup = telebot.types.InlineKeyboardMarkup(row_width=3)
        btn_list = [
            #telebot.types.InlineKeyboardButton(text='Еще место?',
            #                                   callback_data='show_hall_places:{}'.format(playing_place.hall.id)),
            telebot.types.InlineKeyboardButton(text='Завершить?',
                                               callback_data='save_reservation:{}'.format(playing_place.hall.id))
        ]

        reply_markup.add(*btn_list)

        if models.Reservation.objects.filter(playing_place=playing_place, chat_id=chat_id,
                                             is_booked_client=False).count() > 0:
            settings.BOT.send_message(message.from_user.id, 'ВЫ уже выбрали это место', reply_markup=reply_markup)
        else:
            reservation = models.Reservation.objects.filter(chat_id=chat_id, is_booked_client=False).first()
            if reservation.playing_place == None:
                reservation.playing_place = playing_place
                reservation.save()
            else:
                models.Reservation.objects.create(playing_place=playing_place, chat_id=chat_id,
                                                  reservation_date=reservation.reservation_date,
                                                  reservation_time=reservation.reservation_time,
                                                  reservation_end_time=reservation.reservation_end_time,
                                                  price=reservation.price)
        #settings.BOT.send_message(message.from_user.id, 'Выбранные компы {}'.format(', '.join(map(str, [r.playing_place.id for r in models.Reservation.objects.filter(chat_id=chat_id, is_confirmed=False)]))), reply_markup=reply_markup)
        new_message = 'Выбранные компы {}'.format(', '.join(map(str, [r.playing_place.id for r in models.Reservation.objects.filter(chat_id=chat_id, is_confirmed=False)])))
        #show_hall_places(chat_id, playing_place.hall.id, message)
        select_places_edit_message(chat_id, select_item_id=message.message.message_id, message=new_message)
    else:
        #print(message.message.message_id, 'DDDDDDDDDDDD')
        select_places_edit_message(chat_id, select_item_id=message.message.message_id, message='Нельзя забронировать данное место')
        #settings.BOT.send_message(message.from_user.id, 'Нельзя забронировать данное место')


@current_action_save
def show_hall_places(chat_id=None, select_item_id=None, message=None):
    hall = models.Hall.objects.get(id=select_item_id)

    reservation = models.Reservation.objects.filter(chat_id=chat_id, is_booked_client=False).first()

    bot_keyboard = telebot.types.InlineKeyboardMarkup(row_width=8)

    data_list = hall.get_places

    for ll in data_list:
        btn_list = [telebot.types.InlineKeyboardButton(text='{}'.format(play_place_status(place, chat_id)),
                                                       callback_data='place_selected:{}'.format(place.id)) for place
                    in ll]
        bot_keyboard.add(*btn_list)

    settings.BOT.send_message(message.from_user.id, 'Бронирование', reply_markup=bot_keyboard)

def pay_sum_maker(reservations):
    all_sum = 0

    for r in reservations:
        all_sum += r.price.amount * int((r.reservation_end_time.hour - r.reservation_time.hour))

    return all_sum

@current_action_save
def save_reservation(chat_id=None, select_item_id=None, message=None):

    reservations = models.Reservation.objects.filter(chat_id=chat_id, is_booked_client=False)

    bot_keyboard = telebot.types.InlineKeyboardMarkup(row_width=1)

    text_message = 'Вы забронировали на {} компа на {} с {} до {}\nСумма к оплате {}'.format(reservations.count(),
                                                                          reservations.first().reservation_date,
                                                                          reservations.first().reservation_time,
                                                                          reservations.first().reservation_end_time,
                                                                                             pay_sum_maker(reservations))
    btn_list = [telebot.types.InlineKeyboardButton(text='Оплатить', url='http://139.162.172.102:8080/')]
    bot_keyboard.add(*btn_list)

    settings.BOT.send_message(message.from_user.id, text_message, reply_markup=bot_keyboard)

    for reservation in models.Reservation.objects.filter(chat_id=chat_id, is_booked_client=False):
        reservation.is_booked_client = True
        reservation.save()

        for admin in reservation.playing_place.hall.comp_club.employees.all():
            try:
                settings.BOT.send_message(admin.chat_id, 'Новое бронирование')
            except ApiException as e:
                print(e)
